const path = require('path')
function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  baseUrl: process.env.NODE_ENV === 'production' ? './' : '/',
  outputDir: 'dist',
  // eslint-loader 是否在保存的时候检查
  lintOnSave: false,
  //放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
  assetsDir: 'static',
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', resolve('src'))
      .set('config', resolve('src/config'))
      .set('assets', resolve('src/assets'))
      .set('utils', resolve('src/utils'))
      .set('api', resolve('src/api'))
      .set('common', resolve('src/page/common'))
  }
}
