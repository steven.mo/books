
export const config = {
  banner: {
      concentration: "1.1.1.1",
      man: '1.1.1.2',
      woman: '1.1.1.3',
      advertise: '1.1.2.1',
      read_advertise: '1.1.2.2',
      txt_read_advertise: '1.1.2.3'
  },
  blockType: {
      concent: '2.6.1',
      man: '2.6.2',
      woman: '2.6.3'
  },
  block: {
      high: 1,
      good: 2,
      starter: 3,
      work: 4,
      all_look: 5,
      gathering: 6,
      free: 7,
      new_book: 8,
      man_reader: 9,
      man_look: 10,
      pressure: 11,
      new_read: 12,
      hot_over: 13,
      woman_look: 14,
      woman_reader: 15
  },
  errorCodes: {
      NO_TOKEN: 400
  },
  isOver: {
      Over: '3.11.1',
      No_Over: '3.11.2'
  },
  module: {
      UTILS: 'UTILS'
  },
  notice: {
      ALL: "ALL",
      NO_READER: "4.15.1",
      READER: "4.15.2",
      SING_NEWS: '4.15.3',
      COLL_NEWS: '4.15.4'
  },
  suggestionId: 20,
  suggestion: {
      FUNC: "5.20.1",
      MONE: "5.20.2",
      OTHER: "5.20.3"
  },
  setting: {
      SPLIDE: "splide"
  }
}