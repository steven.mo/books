import AuthComponent from 'common/Auth'

const Auth = {
    install (Vue) {
        Vue.component('Auth', AuthComponent)
    }
}
export default Auth