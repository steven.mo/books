import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

//设置时间
import moment from 'moment/moment'
Vue.filter('datetime', function (value, formatString) {
  formatString = formatString || 'YYYY-MM-DD HH:mm:ss';
  return moment(value).format(formatString);
})
Vue.filter('date', function (value, formatString) {
  formatString = formatString || 'YYYY-MM-DD';
  return moment(value).format(formatString);
})

//ele-ui
import eleui from 'config/ElementConfig.js'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(eleui)

import Base from 'utils/Base.js'
Vue.prototype.$base = Base

//设置全局组件
import Auth from 'assets/js/Auth'
Vue.use(Auth)
import Pagination from 'assets/js/Pagination'
Vue.use(Pagination)
import MainPage from 'assets/js/MainPage'
Vue.use(MainPage)

import 'assets/css/styles.css'

router.beforeEach((to, from, next) => {
  const token = store.state.token;
  const loginUrl = store.state.loginUrl ? store.state.loginUrl : "/login";
  if (!token && to.path !== loginUrl) {
      next(loginUrl);
  } else {
      next();
  }
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
