import $axios from 'utils/Request.js'

//列表分页
export const getChapterContentList = data => {
    return $axios({
        url: '/api/chapter_content/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getChapterContentAllList = data => {
    return $axios({
        url: '/api/chapter_content/all',
        method: 'get',
        data
    });
}

//保存
export const saveChapterContent = data => {
    return $axios({
        url: '/api/chapter_content/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteChapterContent = data => {
    return $axios({
        url: '/api/chapter_content/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateChapterContentById = data => {
    return $axios({
        url: '/api/chapter_content/update/' + data.id,
        method: 'put',
        data
    });
}