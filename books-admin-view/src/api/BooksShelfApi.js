import $axios from 'utils/Request.js'

//列表分页
export const getBooksShelfList = data => {
    return $axios({
        url: '/api/books_shelf/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBooksShelfAllList = data => {
    return $axios({
        url: '/api/books_shelf/all',
        method: 'get',
        data
    });
}

//保存
export const saveBooksShelf = data => {
    return $axios({
        url: '/api/books_shelf/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBooksShelf = data => {
    return $axios({
        url: '/api/books_shelf/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBooksShelfById = data => {
    return $axios({
        url: '/api/books_shelf/update/' + data.id,
        method: 'put',
        data
    });
}