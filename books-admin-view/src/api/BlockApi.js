import $axios from 'utils/Request.js'

//列表分页
export const getBlockList = data => {
    return $axios({
        url: '/api/block/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBlockAllList = data => {
    return $axios({
        url: '/api/block/all',
        method: 'get',
        data
    });
}

//保存
export const saveBlock = data => {
    return $axios({
        url: '/api/block/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBlock = data => {
    return $axios({
        url: '/api/block/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBlockById = data => {
    return $axios({
        url: '/api/block/update/' + data.id,
        method: 'put',
        data
    });
}