import $axios from 'utils/Request.js'

//列表分页
export const getSignInList = data => {
    return $axios({
        url: '/api/sign_in/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSignInAllList = data => {
    return $axios({
        url: '/api/sign_in/all',
        method: 'get',
        data
    });
}

//保存
export const saveSignIn = data => {
    return $axios({
        url: '/api/sign_in/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSignIn = data => {
    return $axios({
        url: '/api/sign_in/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSignInById = data => {
    return $axios({
        url: '/api/sign_in/update/' + data.id,
        method: 'put',
        data
    });
}