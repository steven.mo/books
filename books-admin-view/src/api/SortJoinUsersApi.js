import $axios from 'utils/Request.js'

//列表分页
export const getSortJoinUsersList = data => {
    return $axios({
        url: '/api/sort_join_users/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSortJoinUsersAllList = data => {
    return $axios({
        url: '/api/sort_join_users/all',
        method: 'get',
        data
    });
}

//保存
export const saveSortJoinUsers = data => {
    return $axios({
        url: '/api/sort_join_users/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSortJoinUsers = data => {
    return $axios({
        url: '/api/sort_join_users/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSortJoinUsersById = data => {
    return $axios({
        url: '/api/sort_join_users/update/' + data.id,
        method: 'put',
        data
    });
}
