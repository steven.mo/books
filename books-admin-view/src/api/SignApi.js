import $axios from 'utils/Request.js'

//列表分页
export const getSignList = data => {
    return $axios({
        url: '/api/sign/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSignAllList = data => {
    return $axios({
        url: '/api/sign/all',
        method: 'get',
        data
    });
}

//保存
export const saveSign = data => {
    return $axios({
        url: '/api/sign/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSign = data => {
    return $axios({
        url: '/api/sign/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSignById = data => {
    return $axios({
        url: '/api/sign/update/' + data.id,
        method: 'put',
        data
    });
}