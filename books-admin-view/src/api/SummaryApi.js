import $axios from 'utils/Request.js'

export const summaryUser = data => {
    return $axios({
        url: '/api/summary/user',
        method: 'get',
        data
    });
}

export const summaryOrder = data => {
    return $axios({
        url: '/api/summary/order',
        method: 'get',
        data
    });
}


export const summaryLines = data => {
    return $axios({
        url: '/api/summary/lines',
        method: 'get',
        data
    });
}

export const reportLine = data => {
    return $axios({
        url: '/api/summary/report_line',
        method: 'get',
        data
    });
  }