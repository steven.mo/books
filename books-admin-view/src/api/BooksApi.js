import $axios from 'utils/Request.js'

//列表分页
export const getBooksList = data => {
    return $axios({
        url: '/api/books/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBooksAllList = data => {
    return $axios({
        url: '/api/books/all',
        method: 'get',
        data
    });
}

//保存
export const saveBooks = data => {
    return $axios({
        url: '/api/books/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBooks = data => {
    return $axios({
        url: '/api/books/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBooksById = data => {
    return $axios({
        url: '/api/books/update/' + data.id,
        method: 'put',
        data
    });
}