import $axios from 'utils/Request.js'

//列表分页
export const getSortJoinBooksList = data => {
    return $axios({
        url: '/api/sort_join_books/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getSortJoinBooksAllList = data => {
    return $axios({
        url: '/api/sort_join_books/all',
        method: 'get',
        data
    });
}

//保存
export const saveSortJoinBooks = data => {
    return $axios({
        url: '/api/sort_join_books/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteSortJoinBooks = data => {
    return $axios({
        url: '/api/sort_join_books/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateSortJoinBooksById = data => {
    return $axios({
        url: '/api/sort_join_books/update/' + data.id,
        method: 'put',
        data
    });
}

export const getSortIdByBooksId = data => {
    return $axios({
        url: '/api/sort_join_books/getSortIdByBooksId',
        method: 'get',
        data
    });
}