import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let token = '',
	loginUrl = '',
	permissionList = [],
	refreshPage = '',
	homePage = '';
try {
	if (sessionStorage.token)
		token = sessionStorage.token

	if (sessionStorage.loginUrl)
		loginUrl = sessionStorage.loginUrl

	if (sessionStorage.permissionList)
		permissionList = JSON.parse(sessionStorage.permissionList);

	if (sessionStorage.refreshPage)
		refreshPage = sessionStorage.refreshPage
		
	if (sessionStorage.homePage)
	homePage = sessionStorage.homePage
} catch (e) { }

export default new Vuex.Store({
	state: {
		token: token,
		loginUrl: loginUrl,
		permissionList: permissionList,
		refreshPage: refreshPage,
		homePage: homePage
	},
	mutations: {
		saveToken(state, token) {
			state.token = token;
			try {
				sessionStorage.token = token
			} catch (e) { }
		},
		saveLoginUrl(state, url) {
			state.loginUrl = url;
			try {
				sessionStorage.login_url = url;
			} catch (e) { }
		},
		clear(state) {
			try {
				sessionStorage.clear();
			} catch (e) { }
		},
		savePermissionList(state, permissionList) {
			state.permissionList = permissionList;
			try {
				sessionStorage.permissionList = JSON.stringify(permissionList);
			} catch (e) { }
		},
		saveRefreshPage(state, refreshPage) {
			state.refreshPage = refreshPage;
			try {
				sessionStorage.refreshPage = refreshPage;
			} catch (e) { }
		},
		saveHomePage(state, homePage) {
			state.homePage = homePage;
			try {
				sessionStorage.homePage = homePage;
			} catch (e) { }
		},
	},
	actions: {

	}
})
