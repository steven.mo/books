import {config} from 'assets/js/Data'

export const getHttpUrl = data => {
  let httpUrl = process.env.VUE_APP_ROOT_URL + data.url

  if(data.module == undefined)
    return httpUrl

  switch (data.module) {
    case config.module.UTILS:
        httpUrl = process.env.VUE_APP_UTILS_URL + data.url
        break;
    default:
        httpUrl = process.env.VUE_APP_ROOT_URL + data.url
        break;
  }
  return httpUrl;
}