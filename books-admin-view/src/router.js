import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'Home',
      component: resolve => require(['@/page/Home'], resolve),
      children: [
        {
          path: '/summary',
          name: 'Summary',
          component: resolve => require(['@/page/pages/summary/Index'], resolve)
        },
        {
          path: '/list/member',
          name: 'BookList',
          component: resolve => require(['@/page/pages/settings/member/List'], resolve),
          meta: {
            title: ['系统设置', '用户管理']
          }
        },
        {
          path: '/list/roles',
          name: 'rolesList',
          component: resolve => require(['@/page/pages/settings/roles/RolesList'], resolve),
          meta: {
            title: ['系统设置', '角色管理']
          }
        },
        {
          path: '/list/permission',
          name: 'permissionList',
          component: resolve => require(['@/page/pages/settings/permission/List'], resolve),
          meta: {
            title: ['系统设置', '资源管理']
          }
        },
        {
          path: '/list/base/data',
          name: 'BaseDataList',
          component: resolve => require(['@/page/pages/base/base_data/List'], resolve),
          meta: {
            title: ['基础设置', '数据字典']
          }
        },
        {
          path: '/global/settting',
          name: 'GlobalSetting',
          component: resolve => require(['@/page/pages/base/settings/Index'], resolve),
          meta: {
            title: ['基础设置', '全局设置']
          }
        },
        {
          path: '/list/product',
          name: 'ProductList',
          component: resolve => require(['@/page/pages/base/product/List'], resolve),
          meta: {
            title: ['基础设置', '产品管理']
          }
        },
        {
          path: '/list/books',
          name: 'BooksList',
          component: resolve => require(['@/page/pages/books/books/List'], resolve),
          meta: {
            title: ['小说管理', '小说列表']
          }
        },
        {
          path: '/list/:booksId/chapter',
          name: 'ChapterList',
          component: resolve => require(['@/page/pages/books/chapter/List'], resolve),
          meta: {
            title: ['小说管理', '小说目录']
          },
          props: true
        },
        {
          path: '/list/blocks',
          name: 'BlocksList',
          component: resolve => require(['@/page/pages/books/blocks/List'], resolve),
          meta: {
            title: ['小说管理', '模块管理']
          }
        },
        {
          path: '/list/:blocksId/books',
          name: 'BlocksJoinBooksList',
          component: resolve => require(['@/page/pages/books/blocks/BlocksJoinBooksList'], resolve),
          meta: {
            title: ['小说管理', '关联小说']
          }
        },
        {
          path: '/list/sort',
          name: 'SortList',
          component: resolve => require(['@/page/pages/books/sort/List'], resolve),
          meta: {
            title: ['小说管理', '分类管理']
          }
        },
        {
          path: '/list/users',
          name: 'UsersList',
          component: resolve => require(['@/page/pages/users/List'], resolve),
          meta: {
            title: ['会员管理', '会员列表']
          }
        },
        {
          path: '/list/orders',
          name: 'OrdersList',
          component: resolve => require(['@/page/pages/orders/List'], resolve),
          meta: {
            title: ['订单管理', '订单列表']
          }
        },
        {
          path: '/list/banner',
          name: 'BannerList',
          component: resolve => require(['@/page/pages/advertise/banner/List'], resolve),
          meta: {
            title: ['广告管理', '轮播列表']
          }
        },
        {
          path: '/list/advertises',
          name: 'AdvertiseList',
          component: resolve => require(['@/page/pages/advertise/advertises/List'], resolve),
          meta: {
            title: ['广告管理', '广告列表']
          }
        },
        {
          path: '/list/notice',
          name: 'NoticeList',
          component: resolve => require(['@/page/pages/notice/notice/List'], resolve),
          meta: {
            title: ['消息中心', '消息列表']
          }
        },
        {
          path: '/list/suggestion',
          name: 'SuggestionList',
          component: resolve => require(['@/page/pages/notice/suggestion/List'], resolve),
          meta: {
            title: ['消息中心', '意见反馈']
          }
        },
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: resolve => require(['@/page/login/Login'], resolve)
    },
    {
      path: '/',
      redirect: '/login'
    }
  ]
})
