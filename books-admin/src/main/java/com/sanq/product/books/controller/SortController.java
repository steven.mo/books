package com.sanq.product.books.controller;

import org.springframework.web.bind.annotation.PathVariable;

import com.sanq.product.books.entity.vo.SortVo;
import com.sanq.product.books.service.SortService;

import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/sort")
public class SortController {

	@Resource
	private SortService sortService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		SortVo sortVo = sortService.findById(id);

		return sortVo != null ? new Response().success(sortVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody SortVo sortVo) {

		int result = sortService.delete(sortVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, SortVo sortVo, Pagination pagination) {

		Pager<SortVo> pager = sortService.findListByPage(sortVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, SortVo sortVo) {

		List<SortVo> list = sortService.findList(sortVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody SortVo sortVo) {

		int result = sortService.save(sortVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody SortVo sortVo,
        @PathVariable("id") Integer id) {

		int result = sortService.update(sortVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}