package com.sanq.product.books.controller;

import com.sanq.product.books.config.Tables;
import com.sanq.product.books.entity.vo.ChapterVo;
import com.sanq.product.books.kafka.producer.service.BooksMQService;
import com.sanq.product.books.service.ChapterContentService;
import com.sanq.product.books.service.ChapterService;
import com.sanq.product.books.service.GenerateTableKeyService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/chapter")
public class ChapterController {

	@Resource
	private ChapterService chapterService;
	@Resource
	private ChapterContentService chapterContentService;
	@Resource
	private GenerateTableKeyService generateTableKeyService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;
	@Resource
	private BooksMQService booksMQService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		ChapterVo chapterVo = chapterService.findById(id);

		return chapterVo != null ? new Response().success(chapterVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody ChapterVo chapterVo) {

		int result = chapterService.delete(chapterVo);

		if (result != 0) {
			try {
				booksMQService.deleteByChapterId(chapterVo.getId());
			} catch (Exception e){
				e.printStackTrace();
			}
		}

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, ChapterVo chapterVo, Pagination pagination) {

		Pager<ChapterVo> pager = chapterService.findListByPage(chapterVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, ChapterVo chapterVo) {

		List<ChapterVo> list = chapterService.findList(chapterVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody ChapterVo chapterVo) {

		Integer id = generateTableKeyService.getTableKey(Tables.chapter.getTable());


		int prevId = chapterService.findPrevChapter(chapterVo.getBookId(), id);


		chapterVo.setId(id);
		chapterVo.setLastChapterId(prevId);
		chapterVo.setNextChapterId(id + 1);

		int result = chapterService.save(chapterVo);

		if (result != 0) {
			chapterVo.getChapterContentVo().setChapterId(id);
			chapterContentService.save(chapterVo.getChapterContentVo());
		}

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody ChapterVo chapterVo,
        @PathVariable("id") Integer id) {

		int result = chapterService.update(chapterVo, id);

		chapterContentService.update(chapterVo.getChapterContentVo(), chapterVo.getChapterContentVo().getId());

		return result == 1 ? new Response().success() : new Response().failure();
	}
}