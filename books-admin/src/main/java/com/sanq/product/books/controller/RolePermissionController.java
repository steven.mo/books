package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.RolePermissionVo;
import com.sanq.product.books.service.RolePermissionService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/role_permission")
public class RolePermissionController {

    @Resource
    private RolePermissionService rolePermissionService;

    @LogAnnotation(description = "通过ID得到详情")
    @RequestMapping(value="/get/{id}",method= RequestMethod.GET)
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        RolePermissionVo rolePermissionVo = rolePermissionService.findById(id);

        return rolePermissionVo != null ? new Response().success(rolePermissionVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @RequestMapping(value="/delete",method= RequestMethod.DELETE)
        public Response deleteById(HttpServletRequest request, @RequestBody RolePermissionVo rolePermissionVo) {

        int result = rolePermissionService.delete(rolePermissionVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @RequestMapping(value="/list",method= RequestMethod.GET)
    public Response findListByPager(HttpServletRequest request, RolePermissionVo rolePermissionVo, Pagination pagination) {

        Pager<RolePermissionVo> pager = rolePermissionService.findListByPage(rolePermissionVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "通过RoleId查询授权的资源")
    @RequestMapping(value="/findPermissionIdByRoleId",method= RequestMethod.GET)
    public Response findPermissionIdByRoleId(HttpServletRequest request, Integer roleId) {

        List<Integer> permissionIds = rolePermissionService.findPermissionIdByRoleId(roleId);

        return new Response().success(permissionIds);
    }

    @LogAnnotation(description = "查询所有数据")
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public Response findList(HttpServletRequest request, RolePermissionVo rolePermissionVo) {

        List<RolePermissionVo> list = rolePermissionService.findList(rolePermissionVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @RequestMapping(value="/save",method= RequestMethod.POST)
    public Response add(HttpServletRequest request, @RequestBody RolePermissionVo rolePermissionVo) {

        if(rolePermissionVo == null || rolePermissionVo.getPermissionIds() == null || rolePermissionVo.getPermissionIds().isEmpty())
            return new Response().failure("请选择角色");

        RolePermissionVo param = new RolePermissionVo();
        param.setRoleId(rolePermissionVo.getRoleId());
        rolePermissionService.delete(param);

        List<RolePermissionVo> list = new ArrayList<>(rolePermissionVo.getPermissionIds().size());

        for(Integer permissionId : rolePermissionVo.getPermissionIds()) {
            param = new RolePermissionVo();
            param.setRoleId(rolePermissionVo.getRoleId());
            param.setPermissionId(permissionId);
            list.add(param);
        }

        rolePermissionService.saveByList(list);

        return new Response().success();
    }


    @LogAnnotation(description = "通过ID修改数据")
    @RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
    public Response updateByKey(HttpServletRequest request, @RequestBody RolePermissionVo rolePermissionVo, @PathVariable("id") Integer id) {

        int result = rolePermissionService.update(rolePermissionVo, id);

        return result == 1 ? new Response().success() : new Response().failure();
    }
}