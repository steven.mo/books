package com.sanq.product.books.controller;

import org.springframework.web.bind.annotation.PathVariable;

import com.sanq.product.books.entity.vo.NoticeVo;
import com.sanq.product.books.service.NoticeService;

import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/notice")
public class NoticeController {

	@Resource
	private NoticeService noticeService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		NoticeVo noticeVo = noticeService.findById(id);

		return noticeVo != null ? new Response().success(noticeVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody NoticeVo noticeVo) {

		int result = noticeService.delete(noticeVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, NoticeVo noticeVo, Pagination pagination) {

		Pager<NoticeVo> pager = noticeService.findListByPage(noticeVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, NoticeVo noticeVo) {

		List<NoticeVo> list = noticeService.findList(noticeVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody NoticeVo noticeVo) {

		int result = noticeService.save(noticeVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody NoticeVo noticeVo,
        @PathVariable("id") Integer id) {

		int result = noticeService.update(noticeVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}