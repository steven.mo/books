package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.SortJoinBooksVo;
import com.sanq.product.books.kafka.producer.service.BooksMQService;
import com.sanq.product.books.service.SortJoinBooksService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/sort_join_books")
public class SortJoinBooksController {

	@Resource
	private SortJoinBooksService sortJoinBooksService;
	@Resource
	private BooksMQService booksMQService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		SortJoinBooksVo sortJoinBooksVo = sortJoinBooksService.findById(id);

		return sortJoinBooksVo != null ? new Response().success(sortJoinBooksVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody SortJoinBooksVo sortJoinBooksVo) {

		int result = sortJoinBooksService.delete(sortJoinBooksVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, SortJoinBooksVo sortJoinBooksVo, Pagination pagination) {

		Pager<SortJoinBooksVo> pager = sortJoinBooksService.findListByPage(sortJoinBooksVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, SortJoinBooksVo sortJoinBooksVo) {

		List<SortJoinBooksVo> list = sortJoinBooksService.findList(sortJoinBooksVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@RequestMapping(value="/getSortIdByBooksId",method= RequestMethod.GET)
	public Response getSortIdByBooksId(HttpServletRequest request,Integer booksId) {

		List<Integer> sortIdList = sortJoinBooksService.getSortIdByBooksId(booksId);

		return new Response().success(sortIdList);
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody SortJoinBooksVo sortJoinBooksVo) {

		if(sortJoinBooksVo == null || sortJoinBooksVo.getSortIds() == null || sortJoinBooksVo.getSortIds().isEmpty())
			return new Response().failure("请选择分类");

		SortJoinBooksVo param = new SortJoinBooksVo();
		param.setBooksId(sortJoinBooksVo.getBooksId());

		sortJoinBooksService.delete(param);

		List<SortJoinBooksVo> sortJoinBooksVoList = new ArrayList<>();

		sortJoinBooksVo.getSortIds().stream().forEach(item -> {
			SortJoinBooksVo joinBooksVo = new SortJoinBooksVo();
			joinBooksVo.setBooksId(sortJoinBooksVo.getBooksId());
			joinBooksVo.setSortId(item);
			sortJoinBooksVoList.add(joinBooksVo);
		});

		sortJoinBooksService.saveByList(sortJoinBooksVoList);

		booksMQService.save2Es(sortJoinBooksVo.getBooksId(), "S");

//		int result = sortJoinBooksService.save(sortJoinBooksVo);

		return new Response().success();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody SortJoinBooksVo sortJoinBooksVo,
        @PathVariable("id") Integer id) {

		int result = sortJoinBooksService.update(sortJoinBooksVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}