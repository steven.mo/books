package com.sanq.product.books.controller;

import com.sanq.product.books.config.Tables;
import com.sanq.product.books.entity.vo.BooksVo;
import com.sanq.product.books.kafka.producer.service.BooksMQService;
import com.sanq.product.books.service.BooksService;
import com.sanq.product.books.service.GenerateTableKeyService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/books")
public class BooksController {

	@Resource
	private BooksService booksService;
	@Resource
	private BooksMQService booksMQService;
	@Resource
	private GenerateTableKeyService generateTableKeyService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		BooksVo booksVo = booksService.findById(id);

		return booksVo != null ? new Response().success(booksVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody BooksVo booksVo) {

		int result = booksService.delete(booksVo);

		if (result != 0) {
			try {
				booksMQService.deleteByBooksId(booksVo.getId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, BooksVo booksVo, Pagination pagination) {

		Pager<BooksVo> pager = booksService.findListByPage(booksVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, BooksVo booksVo) {

		List<BooksVo> list = booksService.findList(booksVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody BooksVo booksVo) {


		Integer id = generateTableKeyService.getTableKey(Tables.books.getTable());
		booksVo.setId(id);
		int result = booksService.save(booksVo);

		if (result != 0)
			booksMQService.save2Es(id, "C");

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody BooksVo booksVo,
        @PathVariable("id") Integer id) {

		int result = booksService.update(booksVo, id);

		if (result != 0)
			booksMQService.save2Es(id, "U");

		return result == 1 ? new Response().success() : new Response().failure();
	}
}