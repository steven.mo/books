package com.sanq.product.books.controller;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.books.service.MemberService;
import com.sanq.product.books.service.PermissionService;
import com.sanq.product.books.utils.Util;
import com.sanq.product.books.utils.WebUtil;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.string.DigestUtil;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import com.sanq.product.security.annotation.Security;
import com.sanq.product.security.enums.SecurityFieldEnum;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/member")
public class MemberController {

	@Resource
	private MemberService memberService;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private PermissionService permissionService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	@LogAnnotation(description = "通过ID得到详情")
	@RequestMapping(value="/get/{id}",method= RequestMethod.GET)
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		MemberVo memberVo = memberService.findById(id);

		return memberVo != null ? new Response().success(memberVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@RequestMapping(value="/delete",method= RequestMethod.DELETE)
	public Response deleteById(HttpServletRequest request, @RequestBody MemberVo memberVo) {

		int result = memberService.delete(memberVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@RequestMapping(value="/list",method= RequestMethod.GET)
	public Response findListByPager(HttpServletRequest request, MemberVo memberVo, Pagination pagination) {

		Pager<MemberVo> pager = memberService.findListByPage(memberVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@RequestMapping(value="/all",method= RequestMethod.GET)
	public Response findList(HttpServletRequest request, MemberVo memberVo) {

		List<MemberVo> list = memberService.findList(memberVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@RequestMapping(value="/save",method= RequestMethod.POST)
	public Response add(HttpServletRequest request, @RequestBody MemberVo memberVo) {


		memberVo.setLoginName(memberVo.getEmail());
		memberVo.setLoginPwd(DigestUtil.getInstance().md5(memberVo.getLoginName()));

		int result = memberService.save(memberVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
	public Response updateByKey(HttpServletRequest request,
        @RequestBody MemberVo memberVo,
        @PathVariable("id") Integer id) {

		int result = memberService.update(memberVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@Security
	@LogAnnotation(description = "登录")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public Response login(HttpServletRequest request, @RequestBody MemberVo memberVo) {

		if (StringUtil.isEmpty(memberVo.getLoginName()))
			return new Response().failure("登录账号不能为空");

		if (StringUtil.isEmpty(memberVo.getLoginPwd()))
			return new Response().failure("密码不能为空");

		memberVo.setLoginPwd(DigestUtil.getInstance().md5(memberVo.getLoginPwd()));
		MemberVo result = memberService.login(memberVo);

		if (result == null)
			return new Response().failure("登录失败, 请检查用户名和密码");

		if (result.getUserStatus() == null || result.getUserStatus() == 0)
			return new Response().failure("当前账户被锁定");

		String token = DigestUtil.getInstance().md5(result.getLoginName() + "::" + result.getLoginPwd());

		jedisPoolService.set(Redis.ReplaceKey.getSysTokenKey(token), JsonUtil.obj2Json(result), 3600);

		return new Response().success(token);
	}

	@LogAnnotation(description = "通过Token获取用户")
	@RequestMapping(value = "/getInfoByToken", method = RequestMethod.GET)
	public Response getInfoByToken(HttpServletRequest request) {

		return new Response().success(Util.getMemberVo(jedisPoolService, WebUtil.getToken(request))); //WebUtil.getUserByToken(request, jedisPoolService, MemberVo.class);
	}

	@LogAnnotation(description = "修改密码")
	@RequestMapping(value = "/updatePwd", method = RequestMethod.PUT)
	public Response updatePwd(HttpServletRequest request, @RequestBody Map<String, String> map) {
		MemberVo memberVo = Util.getMemberVo(jedisPoolService, map.get(SecurityFieldEnum.TOKEN.name()));

		String newPwd = map.get("newLoginPwd");
		String confirmPwd = map.get("confirmPwd");

		if(StringUtil.isEmpty(newPwd))
			return new Response().failure("新密码不能为空");

		if(StringUtil.isEmpty(confirmPwd))
			return new Response().failure("确认密码不能为空");

		if(!newPwd.equals(confirmPwd))
			return new Response().failure("确认密码和新密码不一致");

		MemberVo params = new MemberVo();
		params.setLoginPwd(DigestUtil.getInstance().md5(newPwd));

		memberService.update(params, memberVo.getId());

		return new Response().success();
	}
}