package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.books.entity.vo.PermissionVo;
import com.sanq.product.books.service.PermissionService;
import com.sanq.product.books.utils.Util;
import com.sanq.product.books.utils.WebUtil;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/permission")
public class PermissionController {

    @Resource
    private PermissionService permissionService;
    @Resource
    private JedisPoolService jedisPoolService;

    @LogAnnotation(description = "通过ID得到详情")
    @RequestMapping(value="/get/{id}",method= RequestMethod.GET)
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        PermissionVo permissionVo = permissionService.findById(id);

        return permissionVo != null ? new Response().success(permissionVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @RequestMapping(value="/delete",method= RequestMethod.DELETE)
        public Response deleteById(HttpServletRequest request, @RequestBody PermissionVo permissionVo) {

        int result = permissionService.delete(permissionVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @RequestMapping(value="/list",method= RequestMethod.GET)
    public Response findListByPager(HttpServletRequest request, PermissionVo permissionVo, Pagination pagination) {

        Pager<PermissionVo> pager = permissionService.findListByPage(permissionVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "查询所有数据")
    @RequestMapping(value="/all",method= RequestMethod.GET)
    public Response findList(HttpServletRequest request, PermissionVo permissionVo) {

        List<PermissionVo> list = permissionService.findList(permissionVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "Tree获取数据")
    @RequestMapping(value="/getPermissionByParentList",method= RequestMethod.GET)
    public Response getPermissionByParentList(HttpServletRequest request, Integer parentId) {

        List<PermissionVo> list = permissionService.getPermissionByParentList(parentId);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "通过Token获取授权资源")
    @RequestMapping(value="/findPermissionByMemberId",method= RequestMethod.GET)
    public Response findPermissionByMemberId(HttpServletRequest request) {
        String token = WebUtil.getToken(request);

        MemberVo memberVo = Util.getMemberVo(jedisPoolService, token);
        memberVo.setToken(token);

        List<PermissionVo> list = permissionService.findPermissionByMemberId(0, memberVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @RequestMapping(value="/save",method= RequestMethod.POST)
    public Response add(HttpServletRequest request, @RequestBody PermissionVo permissionVo) {

        int result = permissionService.save(permissionVo);

        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "通过ID修改数据")
    @RequestMapping(value="/update/{id}",method= RequestMethod.PUT)
    public Response updateByKey(HttpServletRequest request, @RequestBody PermissionVo permissionVo, @PathVariable("id") Integer id) {

        int result = permissionService.update(permissionVo, id);

        return result == 1 ? new Response().success() : new Response().failure();
    }
}