package com.sanq.product.books.controller;

import org.springframework.web.bind.annotation.PathVariable;

import com.sanq.product.books.entity.vo.ProductVo;
import com.sanq.product.books.service.ProductService;

import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	@Resource
	private ProductService productService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		ProductVo productVo = productService.findById(id);

		return productVo != null ? new Response().success(productVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody ProductVo productVo) {

		int result = productService.delete(productVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, ProductVo productVo, Pagination pagination) {

		Pager<ProductVo> pager = productService.findListByPage(productVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, ProductVo productVo) {

		List<ProductVo> list = productService.findList(productVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody ProductVo productVo) {

		int result = productService.save(productVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody ProductVo productVo,
        @PathVariable("id") Integer id) {

		int result = productService.update(productVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}