package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.GlobalSettingVo;
import com.sanq.product.books.model.strategy.setting.SettingFactory;
import com.sanq.product.books.service.GlobalSettingService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.web.JsonUtil;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/api/global_setting")
public class GlobalSettingController {

    @Resource
    private GlobalSettingService globalSettingService;

    @LogAnnotation(description = "通过ID得到详情")
    @GetMapping(value = "/get/{id}")
    public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
        GlobalSettingVo globalSettingVo = globalSettingService.findById(id);

        return globalSettingVo != null ? new Response().success(globalSettingVo) : new Response().failure();
    }

    @LogAnnotation(description = "删除数据")
    @DeleteMapping(value = "/delete")
    public Response deleteById(HttpServletRequest request, @RequestBody GlobalSettingVo globalSettingVo) {

        int result = globalSettingService.delete(globalSettingVo);
        return result == 1 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "分页查询数据")
    @GetMapping(value = "/list")
    public Response findListByPager(HttpServletRequest request, GlobalSettingVo globalSettingVo, Pagination pagination) {

        Pager<GlobalSettingVo> pager = globalSettingService.findListByPage(globalSettingVo, pagination);

        return pager != null ? new Response().success(pager) : new Response().failure();
    }

    @LogAnnotation(description = "查询所有数据")
    @GetMapping(value = "/all")
    public Response findList(HttpServletRequest request, GlobalSettingVo globalSettingVo) {

        List<GlobalSettingVo> list = globalSettingService.findList(globalSettingVo);

        if (list != null && !list.isEmpty()) {
            return new Response().success(SettingFactory.getInstance().getFactory(globalSettingVo.getSettingType()).convert(list.get(0).getSettingContent()));
        }


        return new Response().failure();
    }

    @LogAnnotation(description = "添加数据")
    @PostMapping(value = "/save")
    public Response add(HttpServletRequest request, @RequestBody GlobalSettingVo globalSettingVo) {

        if (globalSettingVo.getSplides() != null && !globalSettingVo.getSplides().isEmpty()) {
            globalSettingVo.setSettingContent(JsonUtil.obj2Json(globalSettingVo.getSplides()));
        }

        int result = globalSettingService.save(globalSettingVo);

        return result != 0 ? new Response().success() : new Response().failure();
    }

    @LogAnnotation(description = "通过ID修改数据")
    @PutMapping(value = "/update/{id}")
    public Response updateByKey(HttpServletRequest request,
                                @RequestBody GlobalSettingVo globalSettingVo,
                                @PathVariable("id") Integer id) {

        int result = globalSettingService.update(globalSettingVo, id);

        return result == 1 ? new Response().success() : new Response().failure();
    }
}