package com.sanq.product.books.controller;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.SuggestionsVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.SuggestionsService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/app/suggestions")
public class SuggestionsController {

	@Resource
	private SuggestionsService suggestionsService;
	@Resource
	private JedisPoolService jedisPoolService;


	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody SuggestionsVo suggestionsVo) {
		UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(suggestionsVo.getToken())), UsersVo.class);


		suggestionsVo.setUserId(usersVo.getId());
		int result = suggestionsService.save(suggestionsVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}