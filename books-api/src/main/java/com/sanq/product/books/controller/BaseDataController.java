package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.BaseDataVo;
import com.sanq.product.books.service.BaseDataService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/app/base_data")
public class BaseDataController {

	@Resource
	private BaseDataService baseDataService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		BaseDataVo baseDataVo = baseDataService.findById(id);

		return baseDataVo != null ? new Response().success(baseDataVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody BaseDataVo baseDataVo) {

		int result = baseDataService.delete(baseDataVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, BaseDataVo baseDataVo, Pagination pagination) {

		Pager<BaseDataVo> pager = baseDataService.findListByPage(baseDataVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, BaseDataVo baseDataVo) {

		List<BaseDataVo> list = baseDataService.findList(baseDataVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody BaseDataVo baseDataVo) {

		int result = baseDataService.save(baseDataVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody BaseDataVo baseDataVo,
        @PathVariable("id") Integer id) {

		int result = baseDataService.update(baseDataVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}