package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.BlockVo;
import com.sanq.product.books.service.BlockService;
import com.sanq.product.books.service.BooksService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.security.annotation.IgnoreSecurity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/app/block")
public class BlockController {

	@Resource
	private BlockService blockService;
	@Resource
	private BooksService booksService;

	@IgnoreSecurity
	@LogAnnotation(description = "通过ID得到详情， 并分页查询出小说")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id, Pagination pagination) {
		BlockVo blockVo = blockService.findById(id);

		if(blockVo != null) {
			blockVo.setBooksVos(booksService.findBooksByBlockId(id, pagination));
		}

		return blockVo != null ? new Response().success(blockVo) : new Response().failure();
	}

}