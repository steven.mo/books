package com.sanq.product.books.controller;

import com.sanq.product.books.entity.vo.PayHistoryVo;
import com.sanq.product.books.service.PayHistoryService;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.entity.Response;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/app/pay_history")
public class PayHistoryController {

	@Resource
	private PayHistoryService payHistoryService;

	@LogAnnotation(description = "通过ID得到详情")
	@GetMapping(value="/get/{id}")
	public Response getById(HttpServletRequest request, @PathVariable("id") Integer id) {
		PayHistoryVo payHistoryVo = payHistoryService.findById(id);

		return payHistoryVo != null ? new Response().success(payHistoryVo) : new Response().failure();
	}

	@LogAnnotation(description = "删除数据")
	@DeleteMapping(value="/delete")
	public Response deleteById(HttpServletRequest request, @RequestBody PayHistoryVo payHistoryVo) {

		int result = payHistoryService.delete(payHistoryVo);
		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "分页查询数据")
	@GetMapping(value="/list")
	public Response findListByPager(HttpServletRequest request, PayHistoryVo payHistoryVo, Pagination pagination) {

		Pager<PayHistoryVo> pager = payHistoryService.findListByPage(payHistoryVo, pagination);

		return pager != null ? new Response().success(pager) : new Response().failure();
	}

	@LogAnnotation(description = "查询所有数据")
	@GetMapping(value="/all")
	public Response findList(HttpServletRequest request, PayHistoryVo payHistoryVo) {

		List<PayHistoryVo> list = payHistoryService.findList(payHistoryVo);
		return list != null ? new Response().success(list) : new Response().failure();
	}

	@LogAnnotation(description = "添加数据")
	@PostMapping(value="/save")
	public Response add(HttpServletRequest request, @RequestBody PayHistoryVo payHistoryVo) {

		int result = payHistoryService.save(payHistoryVo);

		return result == 1 ? new Response().success() : new Response().failure();
	}

	@LogAnnotation(description = "通过ID修改数据")
	@PutMapping(value="/update/{id}")
	public Response updateByKey(HttpServletRequest request,
        @RequestBody PayHistoryVo payHistoryVo,
        @PathVariable("id") Integer id) {

		int result = payHistoryService.update(payHistoryVo, id);

		return result == 1 ? new Response().success() : new Response().failure();
	}
}