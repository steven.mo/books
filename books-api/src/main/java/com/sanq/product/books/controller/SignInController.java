package com.sanq.product.books.controller;

import com.sanq.product.books.config.Common;
import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.SignInVo;
import com.sanq.product.books.entity.vo.SignVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.service.SignInService;
import com.sanq.product.books.service.SignService;
import com.sanq.product.books.service.UsersService;
import com.sanq.product.books.utils.SettingUtil;
import com.sanq.product.books.utils.WebUtil;
import com.sanq.product.config.utils.annotation.LogAnnotation;
import com.sanq.product.config.utils.date.DateUtil;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.config.utils.entity.Response;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.swing.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

@RestController
@RequestMapping("/app/sign_in")
public class SignInController {

    @Resource
    private SignInService signInService;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private SignService signService;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;
    @Resource
    private UsersService usersService;

    @LogAnnotation(description = "查询所有数据")
    @GetMapping(value = "/all")
    public Response findList(HttpServletRequest request, SignInVo signInVo) {

        UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(WebUtil.getToken(request))), UsersVo.class);
        signInVo.setUserId(usersVo.getId());

        List<SignInVo> list = signInService.findList(signInVo);
        return list != null ? new Response().success(list) : new Response().failure();
    }

    @LogAnnotation(description = "验证是否签到")
    @GetMapping(value = "/validateTodayIsSign")
    public Response validateTodayIsSign(HttpServletRequest request) {

        UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(Redis.ReplaceKey.getTokenUser(WebUtil.getToken(request))), UsersVo.class);

        return new Response().success(jedisPoolService.getBit(Redis.ReplaceKey.getSignKey(LocalDateUtils.nowTime()), usersVo.getId()));
    }

    @LogAnnotation(description = "签到")
    @PostMapping(value = "/save")
    public Response add(HttpServletRequest request, @RequestBody SignInVo signInVo) {
        String token = Redis.ReplaceKey.getTokenUser(signInVo.getToken());

        UsersVo usersVo = JsonUtil.json2Obj(jedisPoolService.get(token), UsersVo.class);

        String signKey = Redis.ReplaceKey.getSignKey(LocalDateUtils.nowTime());

        if (jedisPoolService.getBit(signKey, usersVo.getId())) {
            return new Response().failure("今天已经签到过了");
        }

        signInVo.setUserId(usersVo.getId());
        signInVo.setSignDay(LocalDateUtils.nowTime());

        int result = signInService.save(signInVo);
        if (result == 1) {
            jedisPoolService.setBit(signKey, usersVo.getId(), true);
            //设置过期时间  今天到24点 + 第二天的时间
            jedisPoolService.expire(signKey, DateUtil.to24() + (24*3600 + new Random().nextInt(300)));

            taskExecutor.execute(() -> {
                //判断是否为连续签到
                String lastDaySignKey = Redis.ReplaceKey.getSignKey(LocalDateUtils.lastDayStartTime());
                SignVo signVo = new SignVo();
                signVo.setUserId(usersVo.getId());
                signVo.setSignCount(jedisPoolService.getBit(lastDaySignKey, usersVo.getId()) ? 1 : 0);
                signVo.setLastSignTime(Common.DateConvert.getYearAndMonth(signInVo.getSignDay()));
                signService.save(signVo);

                //这里需要进行奖励
                usersVo.setGold(new BigDecimal(SettingUtil.getReward()).add(usersVo.getGold()));
                int update = usersService.update(usersVo, usersVo.getId());

                if (update != 0)
                    jedisPoolService.set(token, JsonUtil.obj2Json(usersVo));

            });

        }

        return result == 1 ? new Response().success() : new Response().failure();
    }

}