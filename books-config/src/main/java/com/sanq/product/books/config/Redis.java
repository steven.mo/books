package com.sanq.product.books.config;

import java.util.Date;

/**
 * com.sanq.product.books.config.Redis
 *
 * @author sanq.Yan
 * @date 2019/7/9
 */
public class Redis {
    private Redis() {
        throw new IllegalStateException("Redis Is No Init");
    }

    public static class RedisKey {
        // token和分类之前的关联key
        public static final String TOKEN_JOIN_SORT = "token:{token}:sort_list";
        // user token
        public static final String USERS_TOKEN = "token:{token}:users";

        //广告
        public static final String ADVERTISE_KEY_DATAKEY = "advertise:{dataKey}";

        //板块小说
        public static final String BOOKS_LIST_BLOCK_PAGE = "books:list:{blockId}:{page}";
        public static final String BLOCK_INFO_ID = "block:info:{id}";
        public static final String SIGN_BIT_KEY = "sign:{date}";

        //
        public static final String SMS_KEY = "sms:{tel}";

        //checkIP
        public static final String BLOCK_IP_SET = "BLOCK_IP_SET";
        public static final String CHECK_IP = "ip:{ip}:{url}";

        //全部分类
        public static final String ALL_SORT_KEY = "all:sort:{blockId}:{sortId}";

        //小说内容
        public static final String CHAPTER_CONTENT_KEY = "chapter:{booksId}:{chapterId}";
        public static final String BOOKS_DETAIL_KEY = "books:{id}:detail";


        //后台
        public static final String SYS_TOKEN_KEY = "sys:{token}:token";
        // 活跃用户
        public static final String ACTIVE_USERS = "active:{date}";
        //后台权限管理
        public static final String PERMISSION_LIST_KEY = "permission:{token}:list";
        //访问信息
        public static final String VISI_LOG_KEY = "visi:log:{date}";

        //全局设置
        public static final String GLOBAL_SETTING_KEY = "setting:{type}";
    }

    public static class ReplaceKey {
        public static String getTokenJoinSort(String token) {
            return RedisKey.TOKEN_JOIN_SORT.replace("{token}", token);
        }

        public static String getTokenUser(String token) {
            return RedisKey.USERS_TOKEN.replace("{token}", token);
        }

        public static String getAdvertiseKey(String dataKey) {
            return RedisKey.ADVERTISE_KEY_DATAKEY.replace("{dataKey}", dataKey);
        }

        public static String getBooksListBlockPage(String blockId, String page) {
            if (blockId == "-1" && page == "-1") {
                return RedisKey.BOOKS_LIST_BLOCK_PAGE
                        .replace("{blockId}", "*")
                        .replace("{page}", "*");
            }
            return RedisKey.BOOKS_LIST_BLOCK_PAGE
                    .replace("{blockId}", blockId + "")
                    .replace("{page}", page + "");
        }

        public static String getBlockId(Integer id) {
            if (id == -1)
                return RedisKey.BLOCK_INFO_ID.replace("{id}", "*");
            return RedisKey.BLOCK_INFO_ID.replace("{id}", id + "");
        }

        public static String getTelCode(String tel) {
            return RedisKey.SMS_KEY.replace("{tel}", tel);
        }

        public static String getSignKey(Date date) {
            if (date == null)
                return RedisKey.SIGN_BIT_KEY.replace("{date}", "*");

            return RedisKey.SIGN_BIT_KEY.replace("{date}", Common.DateConvert.getDate(date));
        }

        public static String getCheckIpKey(String ip, String url) {
            if ("".equals(ip))
                return RedisKey.CHECK_IP.replace("{ip}", "*").replace("{url}", "*");

            return RedisKey.CHECK_IP.replace("{ip}", ip).replace("{url}", url);
        }

        public static String getSysTokenKey(String token) {
            return RedisKey.SYS_TOKEN_KEY.replace("{token}", token);
        }

        public static String getActiveUsers(Date date) {
            if (date == null)
                return RedisKey.ACTIVE_USERS.replace("{date}", "*");

            return RedisKey.ACTIVE_USERS.replace("{date}", Common.DateConvert.getDate(date));
        }

        public static String getPermissionListKey(String token) {
            return RedisKey.PERMISSION_LIST_KEY.replace("{token}", token);
        }

        public static String getVisiLogKey(Date date) {
            if (date == null)
                return RedisKey.VISI_LOG_KEY.replace("{date}", "*");

            return RedisKey.VISI_LOG_KEY.replace("{date}", Common.DateConvert.getDate(date));
        }

        public static String getChapterContentKey(Integer booksId, Integer chapterId) {

            if (booksId == -1)
                return RedisKey.CHAPTER_CONTENT_KEY.replace("{booksId}", "*").replace("{chapterId}", chapterId + "");

            if (chapterId == -1)
                return RedisKey.CHAPTER_CONTENT_KEY.replace("{booksId}", booksId + "").replace("{chapterId}", "*");

            return RedisKey.CHAPTER_CONTENT_KEY.replace("{booksId}", booksId + "").replace("{chapterId}", chapterId + "");
        }

        public static String getSortListKey(String blockId, Integer sortId) {
            if (sortId == -1 || sortId == null)
                return RedisKey.ALL_SORT_KEY.replace("{blockId}", blockId).replace("{sortId}", "*");

            return RedisKey.ALL_SORT_KEY.replace("{blockId}", blockId).replace("{sortId}", sortId + "");
        }

        public static String getBooksDetailKey(Integer booksId) {
            if (booksId == -1)
                return RedisKey.BOOKS_DETAIL_KEY.replace("{id}", "*");
            return RedisKey.BOOKS_DETAIL_KEY.replace("{id}", booksId +"");
        }

        public static String getSettingKey(String type) {
            return RedisKey.GLOBAL_SETTING_KEY.replace("{type}", type);
        }

    }

}
