package com.sanq.product.books.config;

/**
 * com.sanq.product.books.config.Tables
 *
 * @author sanq.Yan
 * @date 2019/8/13
 */
public enum Tables {
    chapter("chapter"),
    books("books"),
    visiLog("visi_log");

    private String table;

    Tables(String table) {
        this.table = table;
    }

    public String getTable() {
        return table;
    }}
