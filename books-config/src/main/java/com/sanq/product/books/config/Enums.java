package com.sanq.product.books.config;

public class Enums {

    public static enum SettingEnum {
        SPLIDE("splide");

        private String name;

        SettingEnum(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

}
