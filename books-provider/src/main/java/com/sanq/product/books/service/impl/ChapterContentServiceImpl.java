package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.ChapterContentVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.ChapterContentService;
import com.sanq.product.books.mapper.ChapterContentMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("chapterContentService")
public class ChapterContentServiceImpl implements ChapterContentService {

	@Resource
	private ChapterContentMapper chapterContentMapper;
	
	@Override
	public int save(ChapterContentVo chapterContentVo) {
		return chapterContentMapper.save(chapterContentVo);
	}
	
	@Override
	public int delete(ChapterContentVo chapterContentVo) {
		return chapterContentMapper.delete(chapterContentVo);
	}	
	
	@Override
	public int update(ChapterContentVo chapterContentVo, Integer id) {
		ChapterContentVo oldChapterContentVo = findById(id);
		
		if(null != oldChapterContentVo && null != chapterContentVo) {
			if(null != chapterContentVo.getId()) {
				oldChapterContentVo.setId(chapterContentVo.getId());
			}
			if(null != chapterContentVo.getBookId()) {
				oldChapterContentVo.setBookId(chapterContentVo.getBookId());
			}
			if(null != chapterContentVo.getChapterId()) {
				oldChapterContentVo.setChapterId(chapterContentVo.getChapterId());
			}
			if(null != chapterContentVo.getContent()) {
				oldChapterContentVo.setContent(chapterContentVo.getContent());
			}
			return chapterContentMapper.update(oldChapterContentVo);
		}
		return 0;
	}
	
	@Override
	public ChapterContentVo findById(Integer id) {
		return chapterContentMapper.findById(id);
	}
	
	@Override
	public List<ChapterContentVo> findList(ChapterContentVo chapterContentVo) {
		return chapterContentMapper.findList(chapterContentVo);
	}
	
	@Override
	public Pager<ChapterContentVo> findListByPage(ChapterContentVo chapterContentVo,Pagination pagination) {
		pagination.setTotalCount(findCount(chapterContentVo));
		
		List<ChapterContentVo> datas = chapterContentMapper.findListByPage(chapterContentVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<ChapterContentVo>(pagination,datas);
	}
	
	@Override
	public int findCount(ChapterContentVo chapterContentVo) {
		return chapterContentMapper.findCount(chapterContentVo);
	}
	
	@Override
	public void saveByList(List<ChapterContentVo> chapterContentVos) {
		chapterContentMapper.saveByList(chapterContentVos);
	}
}