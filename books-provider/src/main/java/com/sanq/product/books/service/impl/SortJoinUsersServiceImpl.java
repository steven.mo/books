package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.SortJoinUsersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.SortJoinUsersService;
import com.sanq.product.books.mapper.SortJoinUsersMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("sortJoinUsersService")
public class SortJoinUsersServiceImpl implements SortJoinUsersService {

	@Resource
	private SortJoinUsersMapper sortJoinUsersMapper;
	
	@Override
	public int save(SortJoinUsersVo sortJoinUsersVo) {
		return sortJoinUsersMapper.save(sortJoinUsersVo);
	}
	
	@Override
	public int delete(SortJoinUsersVo sortJoinUsersVo) {
		return sortJoinUsersMapper.delete(sortJoinUsersVo);
	}	
	
	@Override
	public int update(SortJoinUsersVo sortJoinUsersVo, Integer id) {
		SortJoinUsersVo oldSortJoinUsersVo = findById(id);
		
		if(null != oldSortJoinUsersVo && null != sortJoinUsersVo) {
			if(null != sortJoinUsersVo.getId()) {
				oldSortJoinUsersVo.setId(sortJoinUsersVo.getId());
			}
			if(null != sortJoinUsersVo.getSortId()) {
				oldSortJoinUsersVo.setSortId(sortJoinUsersVo.getSortId());
			}
			if(null != sortJoinUsersVo.getUsersId()) {
				oldSortJoinUsersVo.setUsersId(sortJoinUsersVo.getUsersId());
			}
			return sortJoinUsersMapper.update(oldSortJoinUsersVo);
		}
		return 0;
	}
	
	@Override
	public SortJoinUsersVo findById(Integer id) {
		return sortJoinUsersMapper.findById(id);
	}
	
	@Override
	public List<SortJoinUsersVo> findList(SortJoinUsersVo sortJoinUsersVo) {
		return sortJoinUsersMapper.findList(sortJoinUsersVo);
	}
	
	@Override
	public Pager<SortJoinUsersVo> findListByPage(SortJoinUsersVo sortJoinUsersVo,Pagination pagination) {
		pagination.setTotalCount(findCount(sortJoinUsersVo));
		
		List<SortJoinUsersVo> datas = sortJoinUsersMapper.findListByPage(sortJoinUsersVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<SortJoinUsersVo>(pagination,datas);
	}
	
	@Override
	public int findCount(SortJoinUsersVo sortJoinUsersVo) {
		return sortJoinUsersMapper.findCount(sortJoinUsersVo);
	}
	
	@Override
	public void saveByList(List<SortJoinUsersVo> sortJoinUsersVos) {
		sortJoinUsersMapper.saveByList(sortJoinUsersVos);
	}
}