package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.BooksShelfVo;
import com.sanq.product.books.mapper.BooksShelfMapper;
import com.sanq.product.books.service.BooksShelfService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("booksShelfService")
public class BooksShelfServiceImpl implements BooksShelfService {

	@Resource
	private BooksShelfMapper booksShelfMapper;
	
	@Override
	public int save(BooksShelfVo booksShelfVo) {
		return booksShelfMapper.save(booksShelfVo);
	}
	
	@Override
	public int delete(BooksShelfVo booksShelfVo) {
		return booksShelfMapper.delete(booksShelfVo);
	}	
	
	@Override
	public int update(BooksShelfVo booksShelfVo, Integer id) {
		BooksShelfVo oldBooksShelfVo = findById(id);
		
		if(null != oldBooksShelfVo && null != booksShelfVo) {
			if(null != booksShelfVo.getId()) {
				oldBooksShelfVo.setId(booksShelfVo.getId());
			}
			if(null != booksShelfVo.getUserId()) {
				oldBooksShelfVo.setUserId(booksShelfVo.getUserId());
			}
			if(null != booksShelfVo.getBookId()) {
				oldBooksShelfVo.setBookId(booksShelfVo.getBookId());
			}
			return booksShelfMapper.update(oldBooksShelfVo);
		}
		return 0;
	}
	
	@Override
	public BooksShelfVo findById(Integer id) {
		return booksShelfMapper.findById(id);
	}
	
	@Override
	public List<BooksShelfVo> findList(BooksShelfVo booksShelfVo) {
		return booksShelfMapper.findList(booksShelfVo);
	}
	
	@Override
	public Pager<BooksShelfVo> findListByPage(BooksShelfVo booksShelfVo,Pagination pagination) {
		pagination.setTotalCount(findCount(booksShelfVo));
		
		List<BooksShelfVo> datas = booksShelfMapper.findListByPage(booksShelfVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<BooksShelfVo>(pagination,datas);
	}
	
	@Override
	public int findCount(BooksShelfVo booksShelfVo) {
		return booksShelfMapper.findCount(booksShelfVo);
	}
	
	@Override
	public void saveByList(List<BooksShelfVo> booksShelfVos) {
		booksShelfMapper.saveByList(booksShelfVos);
	}
}