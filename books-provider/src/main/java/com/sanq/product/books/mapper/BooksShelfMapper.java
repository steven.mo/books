package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BooksShelfVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BooksShelfMapper {
	
	int save(BooksShelfVo booksShelfVo);
	
	int delete(BooksShelfVo booksShelfVo);
	
	int update(BooksShelfVo booksShelfVo);
	
	BooksShelfVo findById(Integer id);
	
	List<BooksShelfVo> findList(@Param("booksShelf") BooksShelfVo booksShelfVo);
	
	List<BooksShelfVo> findListByPage(@Param("booksShelf") BooksShelfVo booksShelfVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("booksShelf") BooksShelfVo booksShelfVo);

	void saveByList(@Param("booksShelfVos") List<BooksShelfVo> booksShelfVos);
}