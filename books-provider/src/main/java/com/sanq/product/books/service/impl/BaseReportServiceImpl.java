package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.BaseReportVo;
import com.sanq.product.books.mapper.BaseReportMapper;
import com.sanq.product.books.service.BaseReportService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("baseReportService")
public class BaseReportServiceImpl implements BaseReportService {

	@Resource
	private BaseReportMapper baseReportMapper;
	
	@Override
	public int save(BaseReportVo baseReportVo) {
		return baseReportMapper.save(baseReportVo);
	}
	
	@Override
	public int delete(BaseReportVo baseReportVo) {
		return baseReportMapper.delete(baseReportVo);
	}	
	
	@Override
	public int update(BaseReportVo baseReportVo, Integer id) {
		BaseReportVo oldBaseReportVo = findById(id);
		
		if(null != oldBaseReportVo && null != baseReportVo) {
			if(null != baseReportVo.getId()) {
				oldBaseReportVo.setId(baseReportVo.getId());
			}
			if(null != baseReportVo.getDay()) {
				oldBaseReportVo.setDay(baseReportVo.getDay());
			}
			if(null != baseReportVo.getViews()) {
				oldBaseReportVo.setViews(baseReportVo.getViews());
			}
			if(null != baseReportVo.getIpViews()) {
				oldBaseReportVo.setIpViews(baseReportVo.getIpViews());
			}
			if(null != baseReportVo.getActiveViews()) {
				oldBaseReportVo.setActiveViews(baseReportVo.getActiveViews());
			}
			if(null != baseReportVo.getCreateTime()) {
				oldBaseReportVo.setCreateTime(baseReportVo.getCreateTime());
			}
			return baseReportMapper.update(oldBaseReportVo);
		}
		return 0;
	}
	
	@Override
	public BaseReportVo findById(Integer id) {
		return baseReportMapper.findById(id);
	}
	
	@Override
	public List<BaseReportVo> findList(BaseReportVo baseReportVo) {
		return baseReportMapper.findList(baseReportVo);
	}
	
	@Override
	public Pager<BaseReportVo> findListByPage(BaseReportVo baseReportVo,Pagination pagination) {
		pagination.setTotalCount(findCount(baseReportVo));
		
		List<BaseReportVo> datas = baseReportMapper.findListByPage(baseReportVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<BaseReportVo>(pagination,datas);
	}
	
	@Override
	public int findCount(BaseReportVo baseReportVo) {
		return baseReportMapper.findCount(baseReportVo);
	}
	
	@Override
	public void saveByList(List<BaseReportVo> baseReportVos) {
		baseReportMapper.saveByList(baseReportVos);
	}

	@Override
	public List<Map<String, Object>> findLineDataGroupTime(char a) {
		return baseReportMapper.findLineDataGroupTime(a);
	}
}