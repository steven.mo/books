package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.DiscussVo;
import com.sanq.product.books.mapper.DiscussMapper;
import com.sanq.product.books.service.DiscussService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("discussService")
public class DiscussServiceImpl implements DiscussService {

	@Resource
	private DiscussMapper discussMapper;
	
	@Override
	public int save(DiscussVo discussVo) {
		return discussMapper.save(discussVo);
	}
	
	@Override
	public int delete(DiscussVo discussVo) {
		return discussMapper.delete(discussVo);
	}	
	
	@Override
	public int update(DiscussVo discussVo, Integer id) {
		DiscussVo oldDiscussVo = findById(id);
		
		if(null != oldDiscussVo && null != discussVo) {
			if(null != discussVo.getId()) {
				oldDiscussVo.setId(discussVo.getId());
			}
			if(null != discussVo.getBookId()) {
				oldDiscussVo.setBookId(discussVo.getBookId());
			}
			if(null != discussVo.getStar()) {
				oldDiscussVo.setStar(discussVo.getStar());
			}
			if(null != discussVo.getDiscussContent()) {
				oldDiscussVo.setDiscussContent(discussVo.getDiscussContent());
			}
			if(null != discussVo.getUserId()) {
				oldDiscussVo.setUserId(discussVo.getUserId());
			}
			if(null != discussVo.getCreateTime()) {
				oldDiscussVo.setCreateTime(discussVo.getCreateTime());
			}
			return discussMapper.update(oldDiscussVo);
		}
		return 0;
	}
	
	@Override
	public DiscussVo findById(Integer id) {
		return discussMapper.findById(id);
	}
	
	@Override
	public List<DiscussVo> findList(DiscussVo discussVo) {
		return discussMapper.findList(discussVo);
	}
	
	@Override
	public Pager<DiscussVo> findListByPage(DiscussVo discussVo,Pagination pagination) {
		pagination.setTotalCount(findCount(discussVo));
		
		List<DiscussVo> datas = discussMapper.findListByPage(discussVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<DiscussVo>(pagination,datas);
	}
	
	@Override
	public int findCount(DiscussVo discussVo) {
		return discussMapper.findCount(discussVo);
	}
	
	@Override
	public void saveByList(List<DiscussVo> discussVos) {
		discussMapper.saveByList(discussVos);
	}

	@Override
	public Map<String, Object> findTotalsByBookId(Integer bookId) {
		return discussMapper.findTotalsByBookId(bookId);
	}
}