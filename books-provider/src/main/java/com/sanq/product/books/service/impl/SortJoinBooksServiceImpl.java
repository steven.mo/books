package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.SortJoinBooksVo;
import com.sanq.product.books.mapper.SortJoinBooksMapper;
import com.sanq.product.books.service.SortJoinBooksService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("sortJoinBooksService")
public class SortJoinBooksServiceImpl implements SortJoinBooksService {

	@Resource
	private SortJoinBooksMapper sortJoinBooksMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	/**
	 * 和BooksService中一致
	 * @param booksId
	 */
	public void deleteRedis(Integer booksId) {
		taskExecutor.execute(() -> {
			String key = Redis.ReplaceKey.getBooksDetailKey(booksId);
			jedisPoolService.delete(key);
		});
	}

	@Override
	public int save(SortJoinBooksVo sortJoinBooksVo) {
		return sortJoinBooksMapper.save(sortJoinBooksVo);
	}
	
	@Override
	public int delete(SortJoinBooksVo sortJoinBooksVo) {
		int delete = sortJoinBooksMapper.delete(sortJoinBooksVo);
		if (delete != 0)
			deleteRedis(sortJoinBooksVo.getBooksId());
		return delete;
	}	
	
	@Override
	public int update(SortJoinBooksVo sortJoinBooksVo, Integer id) {
		SortJoinBooksVo oldSortJoinBooksVo = findById(id);
		
		if(null != oldSortJoinBooksVo && null != sortJoinBooksVo) {
			if(null != sortJoinBooksVo.getId()) {
				oldSortJoinBooksVo.setId(sortJoinBooksVo.getId());
			}
			if(null != sortJoinBooksVo.getSortId()) {
				oldSortJoinBooksVo.setSortId(sortJoinBooksVo.getSortId());
			}
			if(null != sortJoinBooksVo.getBooksId()) {
				oldSortJoinBooksVo.setBooksId(sortJoinBooksVo.getBooksId());
			}
			return sortJoinBooksMapper.update(oldSortJoinBooksVo);
		}
		return 0;
	}
	
	@Override
	public SortJoinBooksVo findById(Integer id) {
		return sortJoinBooksMapper.findById(id);
	}
	
	@Override
	public List<SortJoinBooksVo> findList(SortJoinBooksVo sortJoinBooksVo) {
		return sortJoinBooksMapper.findList(sortJoinBooksVo);
	}
	
	@Override
	public Pager<SortJoinBooksVo> findListByPage(SortJoinBooksVo sortJoinBooksVo,Pagination pagination) {
		pagination.setTotalCount(findCount(sortJoinBooksVo));
		
		List<SortJoinBooksVo> datas = sortJoinBooksMapper.findListByPage(sortJoinBooksVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<SortJoinBooksVo>(pagination,datas);
	}
	
	@Override
	public int findCount(SortJoinBooksVo sortJoinBooksVo) {
		return sortJoinBooksMapper.findCount(sortJoinBooksVo);
	}
	
	@Override
	public void saveByList(List<SortJoinBooksVo> sortJoinBooksVos) {
		sortJoinBooksMapper.saveByList(sortJoinBooksVos);
	}

	@Override
	public List<Integer> getSortIdByBooksId(Integer booksId) {
		return sortJoinBooksMapper.getSortIdByBooksId(booksId);
	}

	@Override
	public String findSortIdList(Integer booksId) {
		return sortJoinBooksMapper.findSortIdList(booksId);
	}
}