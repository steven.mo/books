package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.UsersVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface UsersMapper {
	
	int save(UsersVo usersVo);
	
	int delete(UsersVo usersVo);
	
	int update(UsersVo usersVo);
	
	UsersVo findById(Integer id);
	
	List<UsersVo> findList(@Param("users") UsersVo usersVo);
	
	List<UsersVo> findListByPage(@Param("users") UsersVo usersVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("users") UsersVo usersVo);

	void saveByList(@Param("usersVos") List<UsersVo> usersVos);

    Map<String, Integer> findSummaryUser(ServiceModule module);


    List<Map<String, Object>> findLineDataGroupTime();


}