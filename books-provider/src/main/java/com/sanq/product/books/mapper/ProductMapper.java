package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.ProductVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface ProductMapper {
	
	int save(ProductVo productVo);
	
	int delete(ProductVo productVo);
	
	int update(ProductVo productVo);
	
	ProductVo findById(Integer id);
	
	List<ProductVo> findList(@Param("product")  ProductVo productVo);
	
	List<ProductVo> findListByPage(@Param("product") ProductVo productVo,@Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("product")  ProductVo productVo);

	void saveByList(@Param("productVos") List<ProductVo> productVos);
}