package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.RolePermissionVo;
import com.sanq.product.books.mapper.RolePermissionMapper;
import com.sanq.product.books.service.RolePermissionService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("rolePermissionService")
public class RolePermissionServiceImpl implements RolePermissionService {

	@Resource
	private RolePermissionMapper rolePermissionMapper;
	@Resource
	private JedisPoolService jedisPoolService;
	@Resource
	private ThreadPoolTaskExecutor taskExecutor;

	private void deleteRedis() {
		taskExecutor.execute(() -> {
			jedisPoolService.delete(Redis.ReplaceKey.getPermissionListKey("*"));
		});
	}
	
	@Override
	public int save(RolePermissionVo rolePermissionVo) {
		return rolePermissionMapper.save(rolePermissionVo);
	}
	
	@Override
	public int delete(RolePermissionVo rolePermissionVo) {
		int delete = rolePermissionMapper.delete(rolePermissionVo);

		if (delete != 0)
			deleteRedis();

		return delete;
	}	
	
	@Override
	public int update(RolePermissionVo rolePermissionVo, Integer id) {
		RolePermissionVo oldRolePermissionVo = findById(id);
		
		if(null != oldRolePermissionVo && null != rolePermissionVo) {
			if(null != rolePermissionVo.getId()) {
				oldRolePermissionVo.setId(rolePermissionVo.getId());
			}
			if(null != rolePermissionVo.getPermissionId()) {
				oldRolePermissionVo.setPermissionId(rolePermissionVo.getPermissionId());
			}
			if(null != rolePermissionVo.getRoleId()) {
				oldRolePermissionVo.setRoleId(rolePermissionVo.getRoleId());
			}
			return rolePermissionMapper.update(oldRolePermissionVo);
		}
		return 0;
	}
	
	@Override
	public RolePermissionVo findById(Integer id) {
		return rolePermissionMapper.findById(id);
	}
	
	@Override
	public List<RolePermissionVo> findList(RolePermissionVo rolePermissionVo) {
		return rolePermissionMapper.findList(rolePermissionVo);
	}
	
	@Override
	public Pager<RolePermissionVo> findListByPage(RolePermissionVo rolePermissionVo,Pagination pagination) {
		pagination.setTotalCount(findCount(rolePermissionVo));
		
		List<RolePermissionVo> datas = rolePermissionMapper.findListByPage(rolePermissionVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<RolePermissionVo>(pagination,datas);
	}
	
	@Override
	public int findCount(RolePermissionVo rolePermissionVo) {
		return rolePermissionMapper.findCount(rolePermissionVo);
	}
	
	@Override
	public void saveByList(List<RolePermissionVo> rolePermissionVos) {
		rolePermissionMapper.saveByList(rolePermissionVos);

		deleteRedis();
	}

	@Override
	public List<Integer> findPermissionIdByRoleId(Integer roleId) {
		return rolePermissionMapper.findPermissionIdByRoleId(roleId);
	}
}