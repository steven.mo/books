package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.SuggestionsVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.SuggestionsService;
import com.sanq.product.books.mapper.SuggestionsMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("suggestionsService")
public class SuggestionsServiceImpl implements SuggestionsService {

	@Resource
	private SuggestionsMapper suggestionsMapper;
	
	@Override
	public int save(SuggestionsVo suggestionsVo) {
		return suggestionsMapper.save(suggestionsVo);
	}
	
	@Override
	public int delete(SuggestionsVo suggestionsVo) {
		return suggestionsMapper.delete(suggestionsVo);
	}	
	
	@Override
	public int update(SuggestionsVo suggestionsVo, Integer id) {
		SuggestionsVo oldSuggestionsVo = findById(id);
		
		if(null != oldSuggestionsVo && null != suggestionsVo) {
			if(null != suggestionsVo.getId()) {
				oldSuggestionsVo.setId(suggestionsVo.getId());
			}
			if(null != suggestionsVo.getUserId()) {
				oldSuggestionsVo.setUserId(suggestionsVo.getUserId());
			}
			if(null != suggestionsVo.getSugType()) {
				oldSuggestionsVo.setSugType(suggestionsVo.getSugType());
			}
			if(null != suggestionsVo.getSugContent()) {
				oldSuggestionsVo.setSugContent(suggestionsVo.getSugContent());
			}
			if(null != suggestionsVo.getSugReply()) {
				oldSuggestionsVo.setSugReply(suggestionsVo.getSugReply());
			}
			if(null != suggestionsVo.getCreateTime()) {
				oldSuggestionsVo.setCreateTime(suggestionsVo.getCreateTime());
			}
			if(null != suggestionsVo.getReplyTime()) {
				oldSuggestionsVo.setReplyTime(suggestionsVo.getReplyTime());
			}
			if(null != suggestionsVo.getReplier()) {
				oldSuggestionsVo.setReplier(suggestionsVo.getReplier());
			}
			return suggestionsMapper.update(oldSuggestionsVo);
		}
		return 0;
	}
	
	@Override
	public SuggestionsVo findById(Integer id) {
		return suggestionsMapper.findById(id);
	}
	
	@Override
	public List<SuggestionsVo> findList(SuggestionsVo suggestionsVo) {
		return suggestionsMapper.findList(suggestionsVo);
	}
	
	@Override
	public Pager<SuggestionsVo> findListByPage(SuggestionsVo suggestionsVo,Pagination pagination) {
		pagination.setTotalCount(findCount(suggestionsVo));
		
		List<SuggestionsVo> datas = suggestionsMapper.findListByPage(suggestionsVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<SuggestionsVo>(pagination,datas);
	}
	
	@Override
	public int findCount(SuggestionsVo suggestionsVo) {
		return suggestionsMapper.findCount(suggestionsVo);
	}
	
	@Override
	public void saveByList(List<SuggestionsVo> suggestionsVos) {
		suggestionsMapper.saveByList(suggestionsVos);
	}
}