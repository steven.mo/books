package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.PermissionVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface PermissionMapper {
	
	int save(PermissionVo permissionVo);
	
	int delete(PermissionVo permissionVo);
	
	int update(PermissionVo permissionVo);
	
	PermissionVo findById(Integer id);
	
	List<PermissionVo> findList(@Param("permission") PermissionVo permissionVo);
	
	List<PermissionVo> findListByPage(@Param("permission") PermissionVo permissionVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("permission") PermissionVo permissionVo);

	void saveByList(@Param("permissionVos") List<PermissionVo> permissionVos);

	List<PermissionVo> getPermissionByParentList(@Param("id") Integer parentId);

	List<PermissionVo> findPermissionByMemberId(@Param("parentId") Integer parentId, @Param("memberId") Integer memberId);



}