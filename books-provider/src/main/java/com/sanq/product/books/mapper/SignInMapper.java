package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.SignInVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SignInMapper {
	
	int save(SignInVo signInVo);
	
	int delete(SignInVo signInVo);
	
	int update(SignInVo signInVo);
	
	SignInVo findById(Integer id);
	
	List<SignInVo> findList(@Param("signIn") SignInVo signInVo);
	
	List<SignInVo> findListByPage(@Param("signIn") SignInVo signInVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("signIn") SignInVo signInVo);

	void saveByList(@Param("signInVos") List<SignInVo> signInVos);

    void deleteLastMonth(ServiceModule module);

}