package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.SuggestionsVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SuggestionsMapper {
	
	int save(SuggestionsVo suggestionsVo);
	
	int delete(SuggestionsVo suggestionsVo);
	
	int update(SuggestionsVo suggestionsVo);
	
	SuggestionsVo findById(Integer id);
	
	List<SuggestionsVo> findList(@Param("suggestions")  SuggestionsVo suggestionsVo);
	
	List<SuggestionsVo> findListByPage(@Param("suggestions") SuggestionsVo suggestionsVo,@Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("suggestions")  SuggestionsVo suggestionsVo);

	void saveByList(@Param("suggestionsVos") List<SuggestionsVo> suggestionsVos);
}