package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.BlockJoinBooksVo;
import com.sanq.product.books.entity.vo.BooksVo;
import com.sanq.product.books.mapper.BlockJoinBooksMapper;
import com.sanq.product.books.mapper.BooksMapper;
import com.sanq.product.books.service.BooksService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.apache.http.nio.pool.NIOConnFactory;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("booksService")
public class BooksServiceImpl implements BooksService {

    @Resource
    private BooksMapper booksMapper;
    @Resource
    private BlockJoinBooksMapper blockJoinBooksMapper;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    public void deleteRedis(Integer booksId) {
        taskExecutor.execute(() -> {
            String key = Redis.ReplaceKey.getBooksDetailKey(booksId);
            jedisPoolService.delete(key);
        });
    }

    @Override
    public int save(BooksVo booksVo) {
        return booksMapper.save(booksVo);
    }

    @Override
    public int delete(BooksVo booksVo) {
        int delete = booksMapper.delete(booksVo);
        if (delete != 0) {
            if (booksVo.getId() != null) {
                deleteRedis(booksVo.getId());
            } else deleteRedis(-1);
        }
        return delete;
    }

    @Override
    public int update(BooksVo booksVo, Integer id) {
        BooksVo oldBooksVo = findById(id);

        if (null != oldBooksVo && null != booksVo) {
            if (null != booksVo.getId()) {
                oldBooksVo.setId(booksVo.getId());
            }
            if (null != booksVo.getBooksName()) {
                oldBooksVo.setBooksName(booksVo.getBooksName());
            }
            if (null != booksVo.getAuthor()) {
                oldBooksVo.setAuthor(booksVo.getAuthor());
            }
            if (null != booksVo.getSortId()) {
                oldBooksVo.setSortId(booksVo.getSortId());
            }
            if (null != booksVo.getBooksCover()) {
                oldBooksVo.setBooksCover(booksVo.getBooksCover());
            }
            if (null != booksVo.getBooksDescription()) {
                oldBooksVo.setBooksDescription(booksVo.getBooksDescription());
            }
            if (null != booksVo.getCreateTime()) {
                oldBooksVo.setCreateTime(booksVo.getCreateTime());
            }
            if (!StringUtil.isEmpty(booksVo.getIsOver())) {
                oldBooksVo.setIsOver(booksVo.getIsOver());
            }
            if (!StringUtil.isEmpty(booksVo.getReaderSex())) {
                oldBooksVo.setReaderSex(booksVo.getReaderSex());
            }

            int update = booksMapper.update(oldBooksVo);
            if (update != -1)
                deleteRedis(id);
            return update;
        }
        return 0;
    }

    @Override
    public BooksVo findById(Integer id) {
        String key = Redis.ReplaceKey.getBooksDetailKey(id);
        String json = jedisPoolService.get(key);

        BooksVo booksVo;
        if (StringUtil.isEmpty(json)) {
            booksVo = booksMapper.findById(id);
            if (booksVo != null)
                jedisPoolService.set(key, JsonUtil.obj2Json(booksVo));
        } else booksVo = JsonUtil.json2Obj(json, BooksVo.class);

        return booksVo;
    }

    @Override
    public List<BooksVo> findList(BooksVo booksVo) {
        return booksMapper.findList(booksVo);
    }

    @Override
    public Pager<BooksVo> findListByPage(BooksVo booksVo, Pagination pagination) {
        pagination.setTotalCount(findCount(booksVo));

        List<BooksVo> datas = booksMapper.findListByPage(booksVo, pagination.getStartPage(), pagination.getPageSize());

        return new Pager<BooksVo>(pagination, datas);
    }

    @Override
    public int findCount(BooksVo booksVo) {
        return booksMapper.findCount(booksVo);
    }

    @Override
    public void saveByList(List<BooksVo> booksVos) {
        booksMapper.saveByList(booksVos);
    }

    @Override
    public Pager<BooksVo> findBooksByBlockId(Integer blockId, Pagination pagination) {
        String totalKey = Redis.ReplaceKey.getBooksListBlockPage(blockId + "", "-1");
        String listKey = Redis.ReplaceKey.getBooksListBlockPage(blockId + "", pagination.getCurrentIndex() + "");


        String totalJson = jedisPoolService.get(totalKey);
        String listJson = jedisPoolService.get(listKey);

        int totalCount;
        List<BooksVo> datas;

        if (StringUtil.isEmpty(totalJson) || StringUtil.isEmpty(listJson)) {
            BlockJoinBooksVo params = new BlockJoinBooksVo();
            params.setBlockId(blockId);
            totalCount = blockJoinBooksMapper.findCount(params);

            pagination.setTotalCount(totalCount);

            datas = booksMapper.findBooksByBlockId(blockId, pagination.getStartPage(), pagination.getPageSize());
            if (totalCount != 0 && !datas.isEmpty()) {
                jedisPoolService.set(totalKey, totalCount + "");
                jedisPoolService.set(listKey, JsonUtil.obj2Json(datas));
            }
        } else {
            totalCount = StringUtil.toInteger(totalJson);
            pagination.setTotalCount(totalCount);

            datas = JsonUtil.json2ObjList(listJson, BooksVo.class);
        }

        return new Pager<BooksVo>(pagination, datas);
    }


    @Override
    public Pager<BooksVo> findListBySortIds(List<String> sortIds, Pagination pagination) {
        pagination.setTotalCount(findListCountBySortIds(sortIds));

        List<BooksVo> datas = booksMapper.findListBySortIds(sortIds, pagination.getStartPage(), pagination.getPageSize());
        return new Pager<BooksVo>(pagination, datas);
    }

    public int findListCountBySortIds(List<String> sortIds) {
        return booksMapper.findListCountBySortIds(sortIds);
    }
}