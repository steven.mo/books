package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BlockVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface BlockMapper {
	
	int save(BlockVo blockVo);
	
	int delete(BlockVo blockVo);
	
	int update(BlockVo blockVo);
	
	BlockVo findById(Integer id);
	
	List<BlockVo> findList(@Param("block") BlockVo blockVo);
	
	List<BlockVo> findListByPage(@Param("block") BlockVo blockVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("block") BlockVo blockVo);

	void saveByList(@Param("blockVos") List<BlockVo> blockVos);
}