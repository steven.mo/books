package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.BooksVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BooksMapper {
	
	int save(BooksVo booksVo);
	
	int delete(BooksVo booksVo);
	
	int update(BooksVo booksVo);
	
	BooksVo findById(Integer id);
	
	List<BooksVo> findList(@Param("books") BooksVo booksVo);
	
	List<BooksVo> findListByPage(@Param("books") BooksVo booksVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("books") BooksVo booksVo);

	void saveByList(@Param("booksVos") List<BooksVo> booksVos);

	//------------------------
	List<BooksVo> findBooksByBlockId(@Param("blockId") Integer blockId, @Param("startPage") int startPage, @Param("pageSize") int pageSize);

    List<BooksVo> findListBySortIds(@Param("sortIds") List<String> sortIds, @Param("startPage") int startPage, @Param("pageSize")  int pageSize);

	int findListCountBySortIds(@Param("sortIds") List<String> sortIds);
}