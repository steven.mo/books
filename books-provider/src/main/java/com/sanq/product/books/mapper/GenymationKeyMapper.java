package com.sanq.product.books.mapper;

import java.util.Map;

/**
 * Created by XieZhyan on 2018/12/5.
 */
public interface GenymationKeyMapper {
    Map<String,Object> getMaxId(Map<String, Object> param);
}
