package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.OsReportVo;
import com.sanq.product.books.mapper.OsReportMapper;
import com.sanq.product.books.service.OsReportService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("osReportService")
public class OsReportServiceImpl implements OsReportService {

	@Resource
	private OsReportMapper osReportMapper;
	
	@Override
	public int save(OsReportVo osReportVo) {
		return osReportMapper.save(osReportVo);
	}
	
	@Override
	public int delete(OsReportVo osReportVo) {
		return osReportMapper.delete(osReportVo);
	}	
	
	@Override
	public int update(OsReportVo osReportVo, Integer id) {
		OsReportVo oldOsReportVo = findById(id);
		
		if(null != oldOsReportVo && null != osReportVo) {
			if(null != osReportVo.getId()) {
				oldOsReportVo.setId(osReportVo.getId());
			}
			if(null != osReportVo.getDay()) {
				oldOsReportVo.setDay(osReportVo.getDay());
			}
			if(null != osReportVo.getViews()) {
				oldOsReportVo.setViews(osReportVo.getViews());
			}
			if(null != osReportVo.getIpViews()) {
				oldOsReportVo.setIpViews(osReportVo.getIpViews());
			}
			if(null != osReportVo.getOs()) {
				oldOsReportVo.setOs(osReportVo.getOs());
			}
			if(null != osReportVo.getCreateTime()) {
				oldOsReportVo.setCreateTime(osReportVo.getCreateTime());
			}
			return osReportMapper.update(oldOsReportVo);
		}
		return 0;
	}
	
	@Override
	public OsReportVo findById(Integer id) {
		return osReportMapper.findById(id);
	}
	
	@Override
	public List<OsReportVo> findList(OsReportVo osReportVo) {
		return osReportMapper.findList(osReportVo);
	}
	
	@Override
	public Pager<OsReportVo> findListByPage(OsReportVo osReportVo,Pagination pagination) {
		pagination.setTotalCount(findCount(osReportVo));
		
		List<OsReportVo> datas = osReportMapper.findListByPage(osReportVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<OsReportVo>(pagination,datas);
	}
	
	@Override
	public int findCount(OsReportVo osReportVo) {
		return osReportMapper.findCount(osReportVo);
	}
	
	@Override
	public void saveByList(List<OsReportVo> osReportVos) {
		osReportMapper.saveByList(osReportVos);
	}
}