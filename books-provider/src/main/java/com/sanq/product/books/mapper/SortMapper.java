package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.SortVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SortMapper {
	
	int save(SortVo sortVo);
	
	int delete(SortVo sortVo);
	
	int update(SortVo sortVo);
	
	SortVo findById(Integer id);
	
	List<SortVo> findList(@Param("sort") SortVo sortVo);
	
	List<SortVo> findListByPage(@Param("sort") SortVo sortVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("sort") SortVo sortVo);

	void saveByList(@Param("sortVos") List<SortVo> sortVos);
}