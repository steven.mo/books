package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.SignInVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.SignInService;
import com.sanq.product.books.mapper.SignInMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("signInService")
public class SignInServiceImpl implements SignInService {

	@Resource
	private SignInMapper signInMapper;
	
	@Override
	public int save(SignInVo signInVo) {
		return signInMapper.save(signInVo);
	}
	
	@Override
	public int delete(SignInVo signInVo) {
		return signInMapper.delete(signInVo);
	}	
	
	@Override
	public int update(SignInVo signInVo, Integer id) {
		SignInVo oldSignInVo = findById(id);
		
		if(null != oldSignInVo && null != signInVo) {
			if(null != signInVo.getId()) {
				oldSignInVo.setId(signInVo.getId());
			}
			if(null != signInVo.getUserId()) {
				oldSignInVo.setUserId(signInVo.getUserId());
			}
			if(null != signInVo.getSignDay()) {
				oldSignInVo.setSignDay(signInVo.getSignDay());
			}
			if(null != signInVo.getCreateTime()) {
				oldSignInVo.setCreateTime(signInVo.getCreateTime());
			}
			return signInMapper.update(oldSignInVo);
		}
		return 0;
	}
	
	@Override
	public SignInVo findById(Integer id) {
		return signInMapper.findById(id);
	}
	
	@Override
	public List<SignInVo> findList(SignInVo signInVo) {
		return signInMapper.findList(signInVo);
	}
	
	@Override
	public Pager<SignInVo> findListByPage(SignInVo signInVo,Pagination pagination) {
		pagination.setTotalCount(findCount(signInVo));
		
		List<SignInVo> datas = signInMapper.findListByPage(signInVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<SignInVo>(pagination,datas);
	}
	
	@Override
	public int findCount(SignInVo signInVo) {
		return signInMapper.findCount(signInVo);
	}
	
	@Override
	public void saveByList(List<SignInVo> signInVos) {
		signInMapper.saveByList(signInVos);
	}

	@Override
	public void deleteLastMonth(ServiceModule module) {
		signInMapper.deleteLastMonth(module);
	}
}