package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.GlobalSettingVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface GlobalSettingMapper {
	
	int save(GlobalSettingVo globalSettingVo);
	
	int delete(GlobalSettingVo globalSettingVo);
	
	int update(GlobalSettingVo globalSettingVo);
	
	GlobalSettingVo findById(Integer id);
	
	List<GlobalSettingVo> findList(@Param("globalSetting") GlobalSettingVo globalSettingVo);
	
	List<GlobalSettingVo> findListByPage(@Param("globalSetting") GlobalSettingVo globalSettingVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("globalSetting") GlobalSettingVo globalSettingVo);

	void saveByList(@Param("globalSettingVos") List<GlobalSettingVo> globalSettingVos);
}