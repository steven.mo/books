package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.CollectionVo;
import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CollectionMapper {
	
	int save(CollectionVo collectionVo);
	
	int delete(CollectionVo collectionVo);
	
	int update(CollectionVo collectionVo);
	
	CollectionVo findById(Integer id);
	
	List<CollectionVo> findList(@Param("collection") CollectionVo collectionVo);
	
	List<CollectionVo> findListByPage(@Param("collection") CollectionVo collectionVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("collection") CollectionVo collectionVo);

	void saveByList(@Param("collectionVos") List<CollectionVo> collectionVos);
}