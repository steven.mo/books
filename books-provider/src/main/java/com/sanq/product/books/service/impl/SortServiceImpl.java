package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.SortVo;
import com.sanq.product.books.mapper.SortMapper;
import com.sanq.product.books.service.SortService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("sortService")
public class SortServiceImpl implements SortService {

    @Resource
    private SortMapper sortMapper;
    @Resource
    private JedisPoolService jedisPoolService;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    private void deleteRedis(String blockId, Integer sortId) {
        taskExecutor.execute(() -> {
            String key = Redis.ReplaceKey.getSortListKey(blockId, sortId);
            if ("*".equals(blockId) || sortId == -1)
                jedisPoolService.deletes(key);
            else
                jedisPoolService.delete(key);
        });
    }


    @Override
    public int save(SortVo sortVo) {
        int save = sortMapper.save(sortVo);
        if (save != 0)
            deleteRedis(sortVo.getBlockId(), sortVo.getParentId());
        return save;
    }

    @Override
    public int delete(SortVo sortVo) {
        int delete = sortMapper.delete(sortVo);
        if (delete != 0)
            deleteRedis("*", sortVo.getParentId());
        return delete;
    }

    @Override
    public int update(SortVo sortVo, Integer id) {
        SortVo oldSortVo = findById(id);

        if (null != oldSortVo && null != sortVo) {
            if (null != sortVo.getId()) {
                oldSortVo.setId(sortVo.getId());
            }
            if (!StringUtil.isEmpty(sortVo.getBlockId())) {
                oldSortVo.setBlockId(sortVo.getBlockId());
            }
            if (null != sortVo.getParentId()) {
                oldSortVo.setParentId(sortVo.getParentId());
            }
            if (null != sortVo.getSortName()) {
                oldSortVo.setSortName(sortVo.getSortName());
            }
            if (null != sortVo.getCreateTime()) {
                oldSortVo.setCreateTime(sortVo.getCreateTime());
            }
            int update = sortMapper.update(oldSortVo);

            if (update != 0)
                deleteRedis(oldSortVo.getBlockId(), oldSortVo.getParentId());

            return update;
        }
        return 0;
    }

    @Override
    public SortVo findById(Integer id) {
        return sortMapper.findById(id);
    }

    @Override
    public List<SortVo> findList(SortVo sortVo) {
        String key =  Redis.ReplaceKey.getSortListKey(StringUtil.isEmpty(sortVo.getBlockId()) ? "-1" : sortVo.getBlockId(), sortVo.getParentId());
        String json = jedisPoolService.get(key);

        List<SortVo> sortVos;
        if (StringUtil.isEmpty(json)) {
            sortVos = sortMapper.findList(sortVo);
            if (sortVos != null && !sortVos.isEmpty())
                jedisPoolService.set(key, JsonUtil.obj2Json(sortVos));
        } else sortVos = JsonUtil.json2ObjList(json, SortVo.class);

        return sortVos;
    }

    @Override
    public Pager<SortVo> findListByPage(SortVo sortVo, Pagination pagination) {
        pagination.setTotalCount(findCount(sortVo));

        List<SortVo> datas = sortMapper.findListByPage(sortVo, pagination.getStartPage(), pagination.getPageSize());

        return new Pager<SortVo>(pagination, datas);
    }

    @Override
    public int findCount(SortVo sortVo) {
        return sortMapper.findCount(sortVo);
    }

    @Override
    public void saveByList(List<SortVo> sortVos) {
        sortMapper.saveByList(sortVos);
        deleteRedis("*", -1);
    }
}