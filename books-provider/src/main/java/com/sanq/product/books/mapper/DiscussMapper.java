package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.DiscussVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface DiscussMapper {
	
	int save(DiscussVo discussVo);
	
	int delete(DiscussVo discussVo);
	
	int update(DiscussVo discussVo);
	
	DiscussVo findById(Integer id);
	
	List<DiscussVo> findList(@Param("discuss") DiscussVo discussVo);
	
	List<DiscussVo> findListByPage(@Param("discuss") DiscussVo discussVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("discuss") DiscussVo discussVo);

	void saveByList(@Param("discussVos") List<DiscussVo> discussVos);

    Map<String, Object> findTotalsByBookId(Integer bookId);

}