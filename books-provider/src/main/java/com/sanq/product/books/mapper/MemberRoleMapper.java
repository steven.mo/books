package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.MemberRoleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MemberRoleMapper {
	
	int save(MemberRoleVo memberRoleVo);
	
	int delete(MemberRoleVo memberRoleVo);
	
	int update(MemberRoleVo memberRoleVo);
	
	MemberRoleVo findById(Integer id);
	
	List<MemberRoleVo> findList(@Param("memberRole") MemberRoleVo memberRoleVo);
	
	List<MemberRoleVo> findListByPage(@Param("memberRole") MemberRoleVo memberRoleVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("memberRole") MemberRoleVo memberRoleVo);

	void saveByList(@Param("memberRoleVos") List<MemberRoleVo> memberRoleVos);

    List<Integer> findRoleIdListByMemberId(Integer memberId);

}