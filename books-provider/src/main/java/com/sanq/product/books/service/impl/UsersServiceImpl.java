package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.mapper.UsersMapper;
import com.sanq.product.books.service.UsersService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;


@Service("usersService")
public class UsersServiceImpl implements UsersService {

	@Resource
	private UsersMapper usersMapper;
	
	@Override
	public int save(UsersVo usersVo) {
		return usersMapper.save(usersVo);
	}
	
	@Override
	public int delete(UsersVo usersVo) {
		return usersMapper.delete(usersVo);
	}	
	
	@Override
	public int update(UsersVo usersVo, Integer id) {
		UsersVo oldUsersVo = findById(id);
		
		if(null != oldUsersVo && null != usersVo) {
			if(null != usersVo.getId()) {
				oldUsersVo.setId(usersVo.getId());
			}
			if(!StringUtil.isEmpty(usersVo.getNickName())) {
				oldUsersVo.setNickName(usersVo.getNickName());
			}
			if(!StringUtil.isEmpty(usersVo.getSex())) {
				oldUsersVo.setSex(usersVo.getSex());
			}
			if(!StringUtil.isEmpty(usersVo.getAvator())) {
				oldUsersVo.setAvator(usersVo.getAvator());
			}
			if(!StringUtil.isEmpty(usersVo.getTel())) {
				oldUsersVo.setTel(usersVo.getTel());
			}
			if(!StringUtil.isEmpty(usersVo.getEmail())) {
				oldUsersVo.setEmail(usersVo.getEmail());
			}
			if(!StringUtil.isEmpty(usersVo.getLoginPwd())) {
				oldUsersVo.setLoginPwd(usersVo.getLoginPwd());
			}
			if(null != usersVo.getCreateTime()) {
				oldUsersVo.setCreateTime(usersVo.getCreateTime());
			}
			if(null != usersVo.getLastLoginTime()) {
				oldUsersVo.setLastLoginTime(usersVo.getLastLoginTime());
			}
			if(null != usersVo.getImei()) {
				oldUsersVo.setImei(usersVo.getImei());
			}
			if(null != usersVo.getPushKey()) {
				oldUsersVo.setPushKey(usersVo.getPushKey());
			}
			if(null != usersVo.getIsVip()) {
				oldUsersVo.setIsVip(usersVo.getIsVip());
			}
			if(null != usersVo.getVipEndTime()) {
				oldUsersVo.setVipEndTime(usersVo.getVipEndTime());
			}
			if(null != usersVo.getGold()) {
				oldUsersVo.setGold(usersVo.getGold());
			}
			if(null != usersVo.getQq()) {
				oldUsersVo.setQq(usersVo.getQq());
			}
			if(null != usersVo.getWeibo()) {
				oldUsersVo.setWeibo(usersVo.getWeibo());
			}
			return usersMapper.update(oldUsersVo);
		}
		return 0;
	}
	
	@Override
	public UsersVo findById(Integer id) {
		return usersMapper.findById(id);
	}
	
	@Override
	public List<UsersVo> findList(UsersVo usersVo) {
		return usersMapper.findList(usersVo);
	}
	
	@Override
	public Pager<UsersVo> findListByPage(UsersVo usersVo,Pagination pagination) {
		pagination.setTotalCount(findCount(usersVo));
		
		List<UsersVo> datas = usersMapper.findListByPage(usersVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<UsersVo>(pagination,datas);
	}
	
	@Override
	public int findCount(UsersVo usersVo) {
		return usersMapper.findCount(usersVo);
	}
	
	@Override
	public void saveByList(List<UsersVo> usersVos) {
		usersMapper.saveByList(usersVos);
	}

	@Override
	public Map<String, Integer> findSummaryUser(ServiceModule module) {
		return usersMapper.findSummaryUser(module);
	}

	@Override
	public List<Map<String, Object>> findLineDataGroupTime() {
		return usersMapper.findLineDataGroupTime();
	}
}