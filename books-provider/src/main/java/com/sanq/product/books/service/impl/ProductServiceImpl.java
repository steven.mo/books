package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.ProductVo;
import com.sanq.product.books.mapper.ProductMapper;
import com.sanq.product.books.service.ProductService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("productService")
public class ProductServiceImpl implements ProductService {

	@Resource
	private ProductMapper productMapper;
	
	@Override
	public int save(ProductVo productVo) {
		return productMapper.save(productVo);
	}
	
	@Override
	public int delete(ProductVo productVo) {
		return productMapper.delete(productVo);
	}	
	
	@Override
	public int update(ProductVo productVo, Integer id) {
		ProductVo oldProductVo = findById(id);
		
		if(null != oldProductVo && null != productVo) {
			if(null != productVo.getId()) {
				oldProductVo.setId(productVo.getId());
			}
			if(null != productVo.getProductName()) {
				oldProductVo.setProductName(productVo.getProductName());
			}
			if(null != productVo.getObtain()) {
				oldProductVo.setObtain(productVo.getObtain());
			}
			if(null != productVo.getExtraObtain()) {
				oldProductVo.setExtraObtain(productVo.getExtraObtain());
			}
			if(null != productVo.getProductType()) {
				oldProductVo.setProductType(productVo.getProductType());
			}
			if(null != productVo.getPrice()) {
				oldProductVo.setPrice(productVo.getPrice());
			}
			if(null != productVo.getCreateTime()) {
				oldProductVo.setCreateTime(productVo.getCreateTime());
			}
			if (null != productVo.getIsHot()) {
				oldProductVo.setIsHot(productVo.getIsHot());
			}
			return productMapper.update(oldProductVo);
		}
		return 0;
	}
	
	@Override
	public ProductVo findById(Integer id) {
		return productMapper.findById(id);
	}
	
	@Override
	public List<ProductVo> findList(ProductVo productVo) {
		return productMapper.findList(productVo);
	}
	
	@Override
	public Pager<ProductVo> findListByPage(ProductVo productVo,Pagination pagination) {
		pagination.setTotalCount(findCount(productVo));
		
		List<ProductVo> datas = productMapper.findListByPage(productVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<ProductVo>(pagination,datas);
	}
	
	@Override
	public int findCount(ProductVo productVo) {
		return productMapper.findCount(productVo);
	}
	
	@Override
	public void saveByList(List<ProductVo> productVos) {
		productMapper.saveByList(productVos);
	}
}