package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.MemberRoleVo;
import com.sanq.product.books.mapper.MemberRoleMapper;
import com.sanq.product.books.service.MemberRoleService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("memberRoleService")
public class MemberRoleServiceImpl implements MemberRoleService {

	@Resource
	private MemberRoleMapper memberRoleMapper;
	
	@Override
	public int save(MemberRoleVo memberRoleVo) {
		return memberRoleMapper.save(memberRoleVo);
	}
	
	@Override
	public int delete(MemberRoleVo memberRoleVo) {
		return memberRoleMapper.delete(memberRoleVo);
	}	
	
	@Override
	public int update(MemberRoleVo memberRoleVo, Integer id) {
		MemberRoleVo oldMemberRoleVo = findById(id);
		
		if(null != oldMemberRoleVo && null != memberRoleVo) {
			if(null != memberRoleVo.getId()) {
				oldMemberRoleVo.setId(memberRoleVo.getId());
			}
			if(null != memberRoleVo.getUserId()) {
				oldMemberRoleVo.setUserId(memberRoleVo.getUserId());
			}
			if(null != memberRoleVo.getRoleId()) {
				oldMemberRoleVo.setRoleId(memberRoleVo.getRoleId());
			}
			return memberRoleMapper.update(oldMemberRoleVo);
		}
		return 0;
	}
	
	@Override
	public MemberRoleVo findById(Integer id) {
		return memberRoleMapper.findById(id);
	}
	
	@Override
	public List<MemberRoleVo> findList(MemberRoleVo memberRoleVo) {
		return memberRoleMapper.findList(memberRoleVo);
	}
	
	@Override
	public Pager<MemberRoleVo> findListByPage(MemberRoleVo memberRoleVo,Pagination pagination) {
		pagination.setTotalCount(findCount(memberRoleVo));
		
		List<MemberRoleVo> datas = memberRoleMapper.findListByPage(memberRoleVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<MemberRoleVo>(pagination,datas);
	}
	
	@Override
	public int findCount(MemberRoleVo memberRoleVo) {
		return memberRoleMapper.findCount(memberRoleVo);
	}
	
	@Override
	public void saveByList(List<MemberRoleVo> memberRoleVos) {
		memberRoleMapper.saveByList(memberRoleVos);
	}

	@Override
	public List<Integer> findRoleIdListByMemberId(Integer memberId) {
		return memberRoleMapper.findRoleIdListByMemberId(memberId);
	}
}