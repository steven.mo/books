package com.sanq.product.books.service.impl;

import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.ChapterVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.books.mapper.ChapterMapper;
import com.sanq.product.books.service.ChapterService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.JsonUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("chapterService")
public class ChapterServiceImpl implements ChapterService {

	@Resource
	private ChapterMapper chapterMapper;
	@Resource
	private JedisPoolService jedisPoolService;

	@Override
	public int save(ChapterVo chapterVo) {
		return chapterMapper.save(chapterVo);
	}
	
	@Override
	public int delete(ChapterVo chapterVo) {
		return chapterMapper.delete(chapterVo);
	}	
	
	@Override
	public int update(ChapterVo chapterVo, Integer id) {
		ChapterVo oldChapterVo = findById(id);
		
		if(null != oldChapterVo && null != chapterVo) {
			if(null != chapterVo.getId()) {
				oldChapterVo.setId(chapterVo.getId());
			}
			if(null != chapterVo.getBookId()) {
				oldChapterVo.setBookId(chapterVo.getBookId());
			}
			if(null != chapterVo.getChapterName()) {
				oldChapterVo.setChapterName(chapterVo.getChapterName());
			}
			if(null != chapterVo.getCostGold()) {
				oldChapterVo.setCostGold(chapterVo.getCostGold());
			}
			if(null != chapterVo.getLastChapterId()) {
				oldChapterVo.setLastChapterId(chapterVo.getLastChapterId());
			}
			if(null != chapterVo.getNextChapterId()) {
				oldChapterVo.setNextChapterId(chapterVo.getNextChapterId());
			}
			if(null != chapterVo.getCreateTime()) {
				oldChapterVo.setCreateTime(chapterVo.getCreateTime());
			}
			return chapterMapper.update(oldChapterVo);
		}
		return 0;
	}
	
	@Override
	public ChapterVo findById(Integer id) {
		return chapterMapper.findById(id);
	}
	
	@Override
	public List<ChapterVo> findList(ChapterVo chapterVo) {
		return chapterMapper.findList(chapterVo);
	}
	
	@Override
	public Pager<ChapterVo> findListByPage(ChapterVo chapterVo,Pagination pagination) {
		pagination.setTotalCount(findCount(chapterVo));
		
		List<ChapterVo> datas = chapterMapper.findListByPage(chapterVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<ChapterVo>(pagination,datas);
	}
	
	@Override
	public int findCount(ChapterVo chapterVo) {
		return chapterMapper.findCount(chapterVo);
	}
	
	@Override
	public void saveByList(List<ChapterVo> chapterVos) {
		chapterMapper.saveByList(chapterVos);
	}

	@Override
	public ChapterVo findTotalByBookId(Integer bookId) {
		return chapterMapper.findTotalByBookId(bookId);
	}

	@Override
	public Integer findChapterByBookId(Integer id, UsersVo usersVo) {
		return chapterMapper.findChapterByBookId(id, usersVo);
	}

	@Override
	public ChapterVo findChapterContentByBookIdAndChapter(Integer booksId, Integer chapterId) {

		String key = Redis.ReplaceKey.getChapterContentKey(booksId, chapterId);
		String json = jedisPoolService.get(key);

		ChapterVo chapterVo;
		if (StringUtil.isEmpty(json)) {
			chapterVo = chapterMapper.findChapterContentByBookIdAndChapter(booksId, chapterId);
			if (chapterVo != null)
				jedisPoolService.set(key, JsonUtil.obj2Json(chapterVo));
		} else chapterVo = JsonUtil.json2Obj(json, ChapterVo.class);

		return chapterVo;
	}

	@Override
	public Integer findPrevChapter(Integer bookId, Integer id) {
		return chapterMapper.findPrevChapter(bookId, id);
	}

	@Override
	public Integer findNextChapter(Integer bookId, Integer id) {
		return chapterMapper.findNextChapter(bookId, id);
	}
}