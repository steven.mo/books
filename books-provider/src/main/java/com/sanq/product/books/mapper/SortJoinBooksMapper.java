package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.SortJoinBooksVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SortJoinBooksMapper {
	
	int save(SortJoinBooksVo sortJoinBooksVo);
	
	int delete(SortJoinBooksVo sortJoinBooksVo);
	
	int update(SortJoinBooksVo sortJoinBooksVo);
	
	SortJoinBooksVo findById(Integer id);
	
	List<SortJoinBooksVo> findList(@Param("sortJoinBooks") SortJoinBooksVo sortJoinBooksVo);
	
	List<SortJoinBooksVo> findListByPage(@Param("sortJoinBooks") SortJoinBooksVo sortJoinBooksVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("sortJoinBooks") SortJoinBooksVo sortJoinBooksVo);

	void saveByList(@Param("sortJoinBooksVos") List<SortJoinBooksVo> sortJoinBooksVos);

    List<Integer> getSortIdByBooksId(Integer booksId);

	String findSortIdList(@Param("booksId") Integer booksId);
}