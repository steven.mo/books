package com.sanq.product.books.mapper;

import com.sanq.product.books.entity.vo.RoleVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper {
	
	int save(RoleVo roleVo);
	
	int delete(RoleVo roleVo);
	
	int update(RoleVo roleVo);
	
	RoleVo findById(Integer id);
	
	List<RoleVo> findList(@Param("role") RoleVo roleVo);
	
	List<RoleVo> findListByPage(@Param("role") RoleVo roleVo, @Param("startPage") int startPage, @Param("pageSize") int pageSize);
	
	int findCount(@Param("role") RoleVo roleVo);

	void saveByList(@Param("roleVos") List<RoleVo> roleVos);
}