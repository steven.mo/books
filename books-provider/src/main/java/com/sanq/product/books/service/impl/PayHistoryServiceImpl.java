package com.sanq.product.books.service.impl;

import com.sanq.product.books.entity.vo.PayHistoryVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.books.service.PayHistoryService;
import com.sanq.product.books.mapper.PayHistoryMapper;
import java.util.List;
import java.math.BigDecimal;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;


@Service("payHistoryService")
public class PayHistoryServiceImpl implements PayHistoryService {

	@Resource
	private PayHistoryMapper payHistoryMapper;
	
	@Override
	public int save(PayHistoryVo payHistoryVo) {
		return payHistoryMapper.save(payHistoryVo);
	}
	
	@Override
	public int delete(PayHistoryVo payHistoryVo) {
		return payHistoryMapper.delete(payHistoryVo);
	}	
	
	@Override
	public int update(PayHistoryVo payHistoryVo, Integer id) {
		PayHistoryVo oldPayHistoryVo = findById(id);
		
		if(null != oldPayHistoryVo && null != payHistoryVo) {
			if(null != payHistoryVo.getId()) {
				oldPayHistoryVo.setId(payHistoryVo.getId());
			}
			if(null != payHistoryVo.getBookId()) {
				oldPayHistoryVo.setBookId(payHistoryVo.getBookId());
			}
			if(null != payHistoryVo.getChapterId()) {
				oldPayHistoryVo.setChapterId(payHistoryVo.getChapterId());
			}
			if(null != payHistoryVo.getUserId()) {
				oldPayHistoryVo.setUserId(payHistoryVo.getUserId());
			}
			if(null != payHistoryVo.getPayPrice()) {
				oldPayHistoryVo.setPayPrice(payHistoryVo.getPayPrice());
			}
			if(null != payHistoryVo.getCreateTime()) {
				oldPayHistoryVo.setCreateTime(payHistoryVo.getCreateTime());
			}
			return payHistoryMapper.update(oldPayHistoryVo);
		}
		return 0;
	}
	
	@Override
	public PayHistoryVo findById(Integer id) {
		return payHistoryMapper.findById(id);
	}
	
	@Override
	public List<PayHistoryVo> findList(PayHistoryVo payHistoryVo) {
		return payHistoryMapper.findList(payHistoryVo);
	}
	
	@Override
	public Pager<PayHistoryVo> findListByPage(PayHistoryVo payHistoryVo,Pagination pagination) {
		pagination.setTotalCount(findCount(payHistoryVo));
		
		List<PayHistoryVo> datas = payHistoryMapper.findListByPage(payHistoryVo,pagination.getStartPage(),pagination.getPageSize());
		
		return new Pager<PayHistoryVo>(pagination,datas);
	}
	
	@Override
	public int findCount(PayHistoryVo payHistoryVo) {
		return payHistoryMapper.findCount(payHistoryVo);
	}
	
	@Override
	public void saveByList(List<PayHistoryVo> payHistoryVos) {
		payHistoryMapper.saveByList(payHistoryVos);
	}
}