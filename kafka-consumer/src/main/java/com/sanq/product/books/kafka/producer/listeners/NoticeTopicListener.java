package com.sanq.product.books.kafka.producer.listeners;

import com.sanq.product.books.entity.vo.NoticeVo;
import com.sanq.product.books.entity.vo.SuggestionsVo;
import com.sanq.product.books.service.NoticeService;
import com.sanq.product.books.service.SuggestionsService;
import com.sanq.product.config.utils.string.StringUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.producer.listeners.NoticeTopicListener
 *
 * @author sanq.Yan
 * @date 2019/8/22
 */
@Component
public class NoticeTopicListener {

    @Resource
    private NoticeService noticeService;
    @Resource
    private SuggestionsService suggestionsService;


    public void reply(String value) {
        Integer suggestionId = StringUtil.toInteger(value);

        SuggestionsVo suggestionsVo = null;

        while (true) {
            if (suggestionsVo != null)
                break;

            suggestionsVo = suggestionsService.findById(suggestionId);
        }

        //添加notice
        NoticeVo noticeVo = new NoticeVo();
        noticeVo.setUserId(suggestionsVo.getUserId());
        noticeVo.setNoticeTitle("意见反馈回复");
        noticeVo.setNoticeContent(suggestionsVo.getSugReply());
        noticeVo.setIsRead("4.15.1");
        noticeVo.setNoticeType("4.15.3");

        noticeService.save(noticeVo);
    }
}
