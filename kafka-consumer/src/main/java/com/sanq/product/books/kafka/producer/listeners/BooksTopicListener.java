package com.sanq.product.books.kafka.producer.listeners;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.*;
import com.sanq.product.books.es.module.vo.BooksSearchVo;
import com.sanq.product.books.es.service.BooksSearchService;
import com.sanq.product.books.service.*;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * com.sanq.product.books.kafka.producer.listeners.BooksTopicListener
 *
 * @author sanq.Yan
 * @date 2019/8/15
 */
@Component
public class BooksTopicListener {

    @Resource
    private BooksService booksService;
    @Resource
    private ChapterService chapterService;
    @Resource
    private SortJoinBooksService sortJoinBooksService;
    @Resource
    private BlockJoinBooksService blockJoinBooksService;
    @Resource
    private BooksShelfService booksShelfService;
    @Resource
    private CollectionService collectionService;
    @Resource
    private DiscussService discussService;
    @Resource
    private HistoryService historyService;
    @Resource
    private PayHistoryService payHistoryService;
    @Resource
    private BooksSearchService booksSearchService;
    @Resource
    private JedisPoolService jedisPoolService;


    public void deleteBooks(String data) {
        Integer booksId = StringUtil.toInteger(data);

        ChapterVo chapterVo = new ChapterVo();
        chapterVo.setBookId(booksId);
        chapterService.delete(chapterVo);

        SortJoinBooksVo sortJoinBooksVo = new SortJoinBooksVo();
        sortJoinBooksVo.setBooksId(booksId);
        sortJoinBooksService.delete(sortJoinBooksVo);

        BlockJoinBooksVo blockJoinBooksVo = new BlockJoinBooksVo();
        blockJoinBooksVo.setBookId(booksId);
        blockJoinBooksService.delete(blockJoinBooksVo);

        BooksShelfVo booksShelfVo = new BooksShelfVo();
        booksShelfVo.setBookId(booksId);
        booksShelfService.delete(booksShelfVo);

        CollectionVo collectionVo = new CollectionVo();
        collectionVo.setBookId(booksId);
        collectionService.delete(collectionVo);

        DiscussVo discussVo = new DiscussVo();
        discussVo.setBookId(booksId);
        discussService.delete(discussVo);

        PayHistoryVo payHistoryVo = new PayHistoryVo();
        payHistoryVo.setBookId(booksId);
        payHistoryService.delete(payHistoryVo);

        HistoryVo historyVo = new HistoryVo();
        historyVo.setBookId(booksId);
        historyService.delete(historyVo);

        //从redis中删除章节内容
        jedisPoolService.deletes(Redis.ReplaceKey.getChapterContentKey(booksId, -1));

        booksSearchService.deleteBooksById(booksId + "");
    }

    public void save2Es(String data) {
        String[] split = data.split(":");

        Integer booksId = StringUtil.toInteger(split[0]);

        BooksVo booksVo = null;

        while (true) {
            if (booksVo != null)
                break;

            booksVo = booksService.findById(booksId);
        }

        BooksSearchVo booksSearchVo;
        switch (split[1]) {
            case "C":
                //保存
                booksSearchVo = new BooksSearchVo();
                BeanUtils.copyProperties(booksVo, booksSearchVo);

                booksSearchService.saveBooks(booksSearchVo);
                break;
            case "U":
                booksSearchVo = new BooksSearchVo();
                BeanUtils.copyProperties(booksVo, booksSearchVo);

                booksSearchService.updateBooks(booksSearchVo);
                //修改
                break;
            case "S":

                booksSearchVo = booksSearchService.findBooksById(booksId + "");
                booksSearchVo.setChildSortId(sortJoinBooksService.findSortIdList(booksId));

                booksSearchService.updateBooks(booksSearchVo);
                break;
        }
    }

    public void deleteChapter(String data) {
        Integer chapterId = StringUtil.toInteger(data);

        PayHistoryVo payHistoryVo = new PayHistoryVo();
        payHistoryVo.setChapterId(chapterId);
        payHistoryService.delete(payHistoryVo);

        jedisPoolService.deletes(Redis.ReplaceKey.getChapterContentKey(-1, chapterId));
    }
}
