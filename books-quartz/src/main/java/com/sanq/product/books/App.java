package com.sanq.product.books;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * com.sanq.product.books.App
 *
 * @author sanq.Yan
 * @date 2019/8/30
 */
public class App {

    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("spring/spring-quartz.xml").start();
    }

}
