package com.sanq.product.books.job;


import com.sanq.product.books.service.SignInService;
import com.sanq.product.books.utils.WebUtil;
import com.sanq.product.config.utils.web.LogUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;

/**
 * 管理签到
 */
@Configuration
@EnableScheduling
public class SignJob {

    @Resource
    private SignInService signInService;

    //删除签到信息
//    @Scheduled(cron = "0 0/1 * * * ? ")//每月1号凌晨1点开始执行
    @Scheduled(cron = "0 0 1 1 1/1 ? ")//每月1号凌晨1点开始执行
    @Async//开启异步方法
    public void deleteSign() {
        LogUtil.getInstance(getClass()).i("删除签到信息");
        signInService.deleteLastMonth(WebUtil.getModule(0));
    }


}
