package com.sanq.product.books.job;

import com.sanq.product.books.config.EsIndexs;
import com.sanq.product.books.config.Redis;
import com.sanq.product.books.entity.vo.AreaReportVo;
import com.sanq.product.books.entity.vo.BaseReportVo;
import com.sanq.product.books.entity.vo.OsReportVo;
import com.sanq.product.books.es.service.VisiLogSearchService;
import com.sanq.product.books.kafka.producer.service.VisiLogMQService;
import com.sanq.product.books.service.AreaReportService;
import com.sanq.product.books.service.BaseReportService;
import com.sanq.product.books.service.OsReportService;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.config.utils.string.StringUtil;
import com.sanq.product.config.utils.web.LogUtil;
import com.sanq.product.redis.service.JedisPoolService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * com.sanq.product.books.job.ReportJob
 *
 * @author sanq.Yan
 * @date 2019/8/30
 */
@Configuration
@EnableScheduling
public class ReportJob {

    @Resource
    private VisiLogMQService visiLogMQService;
    @Resource
    private VisiLogSearchService visiLogSearchService;
    @Resource
    private BaseReportService baseReportService;
    @Resource
    private OsReportService osReportService;
    @Resource
    private AreaReportService areaReportService;
    @Resource
    private JedisPoolService jedisPoolService;

    //每天1点开始执行
//    @Scheduled(cron = "0 0/1 * * * ? ")
    @Scheduled(cron = "0 0 1 1/1 * ? ")
    @Async//开启异步方法
    public void saveReport() {
        //开始对前一天的访问日志进行统计
        LogUtil.getInstance(getClass()).i("开始对前一天的访问日志进行统计");
        Date lastDayStartTime = LocalDateUtils.lastDayStartTime();

        String indexName = EsIndexs.ReplaceIndex.getVisiLogIndex(lastDayStartTime);

        /**
         * 保存基本报表
         */
        BaseReportVo baseReportVo = visiLogSearchService.getBaseReport(indexName);
        if (baseReportVo != null) {
            baseReportVo.setDay(lastDayStartTime);
            baseReportVo.setActiveViews(StringUtil.toInteger(jedisPoolService.bitCount(Redis.ReplaceKey.getActiveUsers(lastDayStartTime))));
            baseReportService.save(baseReportVo);
        }

        List<OsReportVo> osReportVoList = visiLogSearchService.getOsReport(indexName);
        if (osReportVoList != null && !osReportVoList.isEmpty()) {
            osReportVoList.stream().forEach(item -> {
                item.setDay(lastDayStartTime);
            });
            osReportService.saveByList(osReportVoList);
        }

        List<AreaReportVo> areaReportVoList = visiLogSearchService.getAreaReport(indexName);
        if (areaReportVoList != null && !areaReportVoList.isEmpty()) {
            areaReportVoList.stream().forEach(item -> {
                item.setDay(lastDayStartTime);
            });
            areaReportService.saveByList(areaReportVoList);
        }

        //统计结束 删除日志
        visiLogMQService.saveLog2Es("2");

    }

}
