# 享阅读

## 项目背景
1. 本意是想通过该系统对多年所学技能中和学习， 达到融汇贯通的地步。于是出现了该小说系统 

2. 针对该架构拿来即用， 快速开发其他系统

## 项目介绍
享阅读是一款基于SpringMVC + MyBatis实现的小说系统， 目前只有H5端。 前端采用vue的框架cube-ui, 采用前后端分离的结构，系统中针对数据接口安全做了一定的限制， 很大程度上保证了数据的安全性。

## 技术架构
- 核心框架： SpringMVC + MyBatis + Spring
- 数据库： MySQL
- RPC框架： Zookeeper + Dubbo
- 缓存框架： Redis
- 消息机制： Kafka
- 检索系统： ElasticSearch
- Js框架： Vue.js
- UI框架：
  1. 后端： element-ui
  2. 前端： cube-ui

## 软件环境
- JDK1.8
- MySQL5.7

## 准备事项
- 准备
  1. OSS仓库
  2. 短信发送平台
- 修改
  1. 上传文件发送短信在```books-utils```中， 修改```resources/profile/config.*.properties```的配置项
  2. 上传文件和发送短信具体实现在```books-utils/src/main/java/com/**/utils/```下
  3. 也可根据自己的平台进行单独修改


## 博客内容
以上关于环境配置， 接口安全，Kafka、ES的使用都可在我的掘金博客中找到

[谢大大的掘金](https://juejin.im/user/5d358b226fb9a07ebb057152/posts)

[谢先生的GitHub](https://github.com/xiezhyan/X_Util)

## 配图
### H5
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112103_e7f32a19_1367523.png "1.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112134_8a7f8931_1367523.png "2.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112141_d61e9fc2_1367523.png "3.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112148_7ffbf70c_1367523.png "4.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112154_e4095791_1367523.png "5.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112202_04274ea5_1367523.png "6.PNG")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/112209_6e2475ca_1367523.png "7.PNG")

### 后台
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113941_e042bec2_1367523.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113947_8e975397_1367523.png "2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/113955_847e17ec_1367523.png "3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114001_8663691f_1367523.png "4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114008_104120c9_1367523.png "5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114015_8bd69277_1367523.png "6.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114022_3b84abcb_1367523.png "7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114031_a2640085_1367523.png "8.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/1011/114037_05058582_1367523.png "9.png")