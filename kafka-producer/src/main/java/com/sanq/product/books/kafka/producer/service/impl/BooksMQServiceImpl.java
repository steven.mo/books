package com.sanq.product.books.kafka.producer.service.impl;

import com.sanq.product.books.kafka.producer.service.BooksMQService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.kafka.producer.service.BooksMQServiceImpl
 *
 * @author sanq.Yan
 * @date 2019/8/13
 */
@Service("booksMQService")
public class BooksMQServiceImpl implements BooksMQService {

    @Resource
    private KafkaTemplate<String, String> kafkaTemplate;
    @Resource
    private ThreadPoolTaskExecutor taskExecutor;

    @Value("${kafka.books.del}")
    public String kafkaBooksDelTopic;

    @Value("${kafka.chapter.del}")
    public String kafkaChapterDelTopic;

    @Value("${kafka.books.search}")
    public String kafkaBooksSearch;


    @Override
    public void deleteByBooksId(Integer booksId) {
        taskExecutor.execute(() -> {
            kafkaTemplate.send(kafkaBooksDelTopic, booksId + "");
        });
    }

    @Override
    public void deleteByChapterId(Integer chapterId) {
        taskExecutor.execute(() -> {
            kafkaTemplate.send(kafkaChapterDelTopic, chapterId + "");
        });
    }

    @Override
    public void save2Es(Integer booksId, String type) {
        taskExecutor.execute(() -> {
            kafkaTemplate.send(kafkaBooksSearch, booksId + ":" + type);
        });
    }
}
