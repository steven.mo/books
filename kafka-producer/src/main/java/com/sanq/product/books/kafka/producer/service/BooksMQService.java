package com.sanq.product.books.kafka.producer.service;

/**
 * com.sanq.product.books.kafka.producer.service.BooksMQService
 *
 * @author sanq.Yan
 * @date 2019/8/13
 */
public interface BooksMQService {

    //删除小说
    void deleteByBooksId(Integer booksId);

    //删除章节
    void deleteByChapterId(Integer chapterId);

    //添加到ES
    void save2Es(Integer booksId, String type);

}
