package com.sanq.product.books.es;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

public class StartApp {
    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("classpath:spring/profile/spring-dev-application.xml").start();

        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
