package com.sanq.product.books.es.dao.impl;

import com.sanq.product.books.config.EsIndexs;
import com.sanq.product.books.es.module.vo.BooksSearchVo;
import com.sanq.product.books.es.service.BooksSearchService;
import com.sanq.product.utils.es.entity.SearchPager;
import com.sanq.product.utils.es.entity.SearchPagination;
import com.sanq.product.utils.es.support.impl.BaseSearchSupportImpl;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * com.sanq.product.books.es.dao.impl.BooksSearchSupportImpl
 *
 * @author sanq.Yan
 * @date 2019/8/29
 */
@Repository("booksSearchService")
public class BooksSearchSupportImpl extends BaseSearchSupportImpl<BooksSearchVo> implements BooksSearchService {

    private static final String BOOKS_INDEX = EsIndexs.Indexs.BOOKS_INDEX;
    private static final String _DOC = EsIndexs.Indexs._DOC;

    /**
     * 通过ID获取
     *
     * @param id
     * @return
     */
    @Override
    public BooksSearchVo findBooksById(String id) {
        try {
            if (!super.check(BOOKS_INDEX)) {
                this.createIndex();
            }

            return super.findById(BOOKS_INDEX, _DOC, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new BooksSearchVo();
    }

    /**
     * 保存ES
     *
     * @param booksSearchVo
     * @return
     */
    @Override
    public String saveBooks(BooksSearchVo booksSearchVo) {
        try {
            if (!super.check(BOOKS_INDEX)) {
                this.createIndex();
            }

            return super.save(BOOKS_INDEX, _DOC, booksSearchVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 修改
     *
     * @param booksSearchVo
     * @return
     */
    @Override
    public boolean updateBooks(BooksSearchVo booksSearchVo) {
        try {
            if (!super.check(BOOKS_INDEX)) {
                this.createIndex();
            }

            return super.update(BOOKS_INDEX, _DOC, booksSearchVo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean deleteBooksById(String id) {
        try {
            if (!super.check(BOOKS_INDEX)) {
                this.createIndex();
            }

            return super.delete(BOOKS_INDEX, _DOC, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public SearchPager<BooksSearchVo> findBooksListByPager(BooksSearchVo booksSearchVo, SearchPagination pagination) {
        try {
            if (!super.check(BOOKS_INDEX)) {
                this.createIndex();
            }

            return super.findListByPager(BOOKS_INDEX, _DOC, booksSearchVo, pagination);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createIndex() throws Exception {
        return super.createIndex(BOOKS_INDEX, "{\"properties\": {\n" +
                "\"id\": {\n" +
                "\"type\": \"long\"\n" +
                "},\n" +
                "\"booksName\": {\n" +
                "\"type\": \"text\",\n" +
                "\"analyzer\": \"ik_max_word\",\n" +
                "\"search_analyzer\": \"ik_max_word\"" +
                "},\n" +
                "\"author\": {\n" +
                "\"type\": \"text\",\n" +
                "\"analyzer\": \"ik_smart\",\n" +
                "\"search_analyzer\": \"ik_smart\"" +
                "},\n" +
                "\"sortId\": {\n" +
                "\"type\": \"long\"\n" +
                "},\n" +
                "\"isOver\": {\n" +
                "\"type\": \"keyword\"\n" +
                "},\n" +
                "\"readerSex\": {\n" +
                "\"type\": \"keyword\"\n" +
                "},\n" +
                "\"childSortId\": {\n" +
                "\"type\": \"text\",\n" +
                "\"analyzer\": \"ik_max_word\",\n" +
                "\"search_analyzer\": \"ik_max_word\"" +
                "}}}");
    }
}
