package com.sanq.product.books;

import com.sanq.product.books.es.module.vo.BooksSearchVo;
import com.sanq.product.books.es.service.BooksSearchService;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.utils.es.entity.SearchPager;
import com.sanq.product.utils.es.entity.SearchPagination;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.TestApp
 *
 * @author sanq.Yan
 * @date 2019/9/4
 */
@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:spring/profile/spring-dev-application.xml"})
public class TestApp {
    @Resource
    private BooksSearchService booksSearchService;

    @Test
    public void findBooksList() {
        BooksSearchVo booksSearchVo = new BooksSearchVo();
        booksSearchVo.setBooksName("异");
        SearchPagination pagination = new SearchPagination();
        pagination.setPageSize(10);

        SearchPager<BooksSearchVo> booksListByPager = booksSearchService.findBooksListByPager(booksSearchVo, pagination);

        System.out.println(booksListByPager.getData().size());
    }
}
