
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * com.sanq.product.books.es.App
 *
 * @author sanq.Yan
 * @date 2019/8/23
 */
@Component
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"/spring/spring-application.xml"})
public class App {

    @Resource
    private RestHighLevelClient restClient;

    @Test
    public void check() {
        try {
            GetIndexRequest existsRequest = new GetIndexRequest("books");
            boolean exists = restClient.indices().exists(existsRequest, RequestOptions.DEFAULT);
            System.out.println(exists);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
