package com.sanq.product.books.es.module.enums;

public enum ReportEnum {
    PV,
    UV,
    KEY,
    DOC_COUNT;
}
