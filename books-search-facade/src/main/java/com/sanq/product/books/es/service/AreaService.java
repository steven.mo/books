package com.sanq.product.books.es.service;

import java.util.Map;

/**
 * com.sanq.product.books.service.AreaService
 *
 * @author sanq.Yan
 * @date 2019/8/31
 */
public interface AreaService {
    Map<String, String> getCityNameByIp(String ip);
}
