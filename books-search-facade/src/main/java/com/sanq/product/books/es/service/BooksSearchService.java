package com.sanq.product.books.es.service;

import com.sanq.product.books.es.module.vo.BooksSearchVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import com.sanq.product.utils.es.entity.SearchPager;
import com.sanq.product.utils.es.entity.SearchPagination;
import com.sanq.product.utils.es.support.BaseSearchSupport;

/**
 * com.sanq.product.books.es.service.BooksSearchService
 *
 * @author sanq.Yan
 * @date 2019/8/29
 */
public interface BooksSearchService extends BaseSearchSupport<BooksSearchVo> {
    /**
     * 创建索引
     * @return
     * @throws Exception
     */
    boolean createIndex() throws Exception;

    /**
     * 查询列表
     * @param booksSearchVo
     * @param pagination
     * @return
     */
    SearchPager<BooksSearchVo> findBooksListByPager(BooksSearchVo booksSearchVo, SearchPagination pagination);

    /**
     * 删除
     * @param id
     * @return
     */
    boolean deleteBooksById(String id);

    /**
     * 保存ES
     * @param booksSearchVo
     * @return
     */
    String saveBooks(BooksSearchVo booksSearchVo);

    /**
     * 修改
     * @param booksSearchVo
     * @return
     */
    boolean updateBooks(BooksSearchVo booksSearchVo);

    /**
     * 通过ID获取
     * @param id
     * @return
     */
    BooksSearchVo findBooksById(String id);
}
