package com.sanq.product.books.es.module.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.sanq.product.books.entity.vo.BooksVo;
import com.sanq.product.books.es.module.BooksSearch;

import java.util.Map;

public class BooksSearchVo extends BooksSearch {

    @JSONField(serialize = false)
    private BooksVo booksVo;

    public BooksVo getBooksVo() {
        return booksVo;
    }

    public void setBooksVo(BooksVo booksVo) {
        this.booksVo = booksVo;
    }
}
