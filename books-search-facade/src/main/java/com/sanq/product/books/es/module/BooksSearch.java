package com.sanq.product.books.es.module;

import com.sanq.product.books.entity.vo.SortJoinBooksVo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * com.sanq.product.books.es.module.BooksSearchVo
 *
 * @author sanq.Yan
 * @date 2019/9/3
 */
public class BooksSearch implements Serializable {

    /***/
    private Integer id;
    /**
     * 小说名称
     */
    private String booksName;
    /**
     * 作者
     */
    private String author;
    /**
     * 分类
     */
    private Integer sortId;
    /**
     * 0.未完结 1.已完结
     */
    private String isOver;
    /**
     * 阅读人群性别
     */
    private String readerSex;
    /**
     * 子分类
     */
    private String childSortId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBooksName() {
        return booksName;
    }

    public void setBooksName(String booksName) {
        this.booksName = booksName;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Integer getSortId() {
        return sortId;
    }

    public void setSortId(Integer sortId) {
        this.sortId = sortId;
    }

    public String getIsOver() {
        return isOver;
    }

    public void setIsOver(String isOver) {
        this.isOver = isOver;
    }

    public String getReaderSex() {
        return readerSex;
    }

    public void setReaderSex(String readerSex) {
        this.readerSex = readerSex;
    }

    public String getChildSortId() {
        return childSortId;
    }

    public void setChildSortId(String childSortId) {
        this.childSortId = childSortId;
    }
}