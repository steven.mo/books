package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Notice;

public class NoticeVo extends Notice{

	/**
	 *	version: 消息扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-28
	 */
	private static final long serialVersionUID = 1L;

	private String noticeType;

	private UsersVo usersVo;

	public UsersVo getUsersVo() {
		return usersVo;
	}

	public void setUsersVo(UsersVo usersVo) {
		this.usersVo = usersVo;
	}

	public String getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(String noticeType) {
		this.noticeType = noticeType;
	}
}
