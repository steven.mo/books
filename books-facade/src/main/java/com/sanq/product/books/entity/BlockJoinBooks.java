package com.sanq.product.books.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class BlockJoinBooks  implements Serializable {

	/**
	 *	version: 版块和小说关联
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-11
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**版块ID*/
	private Integer blockId;
	/**小说ID*/
	private Integer bookId;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBlockId() {
		return blockId;
	}
	public void setBlockId(Integer blockId) {
		this.blockId = blockId;
	}

	public Integer getBookId() {
		return bookId;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
