package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.SignIn;
import com.sanq.product.config.utils.date.LocalDateUtils;

import java.util.Date;

public class SignInVo extends SignIn{

	/**
	 *	version: 签到表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private Date startTime = LocalDateUtils.monthStartTime();

	private Date endTime = LocalDateUtils.monthEndTime();

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
}
