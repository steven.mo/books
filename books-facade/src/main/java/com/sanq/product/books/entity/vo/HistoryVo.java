package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.History;

public class HistoryVo extends History{

	/**
	 *	version: 阅读历史扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private Integer readChapterCount;

	private BooksVo booksVo;

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}

	public Integer getReadChapterCount() {
		return readChapterCount;
	}

	public void setReadChapterCount(Integer readChapterCount) {
		this.readChapterCount = readChapterCount;
	}
}
