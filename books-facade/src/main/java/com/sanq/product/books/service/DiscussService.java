package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.DiscussVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;
import java.util.Map;

public interface DiscussService {
	
	int save(DiscussVo discussVo);
	
	int delete(DiscussVo discussVo);
	
	int update(DiscussVo discussVo, Integer id);
	
	DiscussVo findById(Integer id);
	
	List<DiscussVo> findList(DiscussVo discussVo);
	
	Pager<DiscussVo> findListByPage(DiscussVo discussVo, Pagination pagination);
	
	int findCount(DiscussVo discussVo);
	
	void saveByList(List<DiscussVo> discussVos);

    Map<String, Object> findTotalsByBookId(Integer bookId);
}