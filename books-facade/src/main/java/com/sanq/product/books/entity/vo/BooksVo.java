package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Books;

import java.util.List;

public class BooksVo extends Books{

	/**
	 *	version: 小说扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private SortVo parentSortVo;

	private List<SortVo> sortVoList;

	private boolean collection;	//是否收藏
	private boolean addToShelf;	//是否添加到书架

	private Integer childSortId;

	private String searchValue;

	private Integer chapterId;	// 如果没有阅读历史， 就是第一章 如果有 就是已经阅读的章节

	public Integer getChapterId() {
		return chapterId;
	}

	public void setChapterId(Integer chapterId) {
		this.chapterId = chapterId;
	}

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public Integer getChildSortId() {
		return childSortId;
	}

	public void setChildSortId(Integer childSortId) {
		this.childSortId = childSortId;
	}

	public boolean isAddToShelf() {
		return addToShelf;
	}

	public void setAddToShelf(boolean addToShelf) {
		this.addToShelf = addToShelf;
	}

	public boolean isCollection() {
		return collection;
	}

	public void setCollection(boolean collection) {
		this.collection = collection;
	}

	public SortVo getParentSortVo() {
		return parentSortVo;
	}

	public void setParentSortVo(SortVo parentSortVo) {
		this.parentSortVo = parentSortVo;
	}

	public List<SortVo> getSortVoList() {
		return sortVoList;
	}

	public void setSortVoList(List<SortVo> sortVoList) {
		this.sortVoList = sortVoList;
	}
}
