package com.sanq.product.books.entity;

import java.io.Serializable;

public class RolePermission  implements Serializable {

	/**
	 *	version: 角色关联表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-23
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	private Integer permissionId;
	/***/
	private Integer roleId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Integer permissionId) {
		this.permissionId = permissionId;
	}

	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}
