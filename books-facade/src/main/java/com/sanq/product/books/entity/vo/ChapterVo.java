package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Chapter;

public class ChapterVo extends Chapter{

	/**
	 *	version: 章节表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private Integer chapterTotalCount;	//总章节

	private ChapterContentVo chapterContentVo;

	private BooksVo booksVo;

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}

	public ChapterContentVo getChapterContentVo() {
		return chapterContentVo;
	}

	public void setChapterContentVo(ChapterContentVo chapterContentVo) {
		this.chapterContentVo = chapterContentVo;
	}

	public Integer getChapterTotalCount() {
		return chapterTotalCount;
	}

	public void setChapterTotalCount(Integer chapterTotalCount) {
		this.chapterTotalCount = chapterTotalCount;
	}
}
