package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.ChapterVo;
import com.sanq.product.books.entity.vo.UsersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface ChapterService {
	
	int save(ChapterVo chapterVo);
	
	int delete(ChapterVo chapterVo);
	
	int update(ChapterVo chapterVo, Integer id);
	
	ChapterVo findById(Integer id);
	
	List<ChapterVo> findList(ChapterVo chapterVo);
	
	Pager<ChapterVo> findListByPage(ChapterVo chapterVo, Pagination pagination);
	
	int findCount(ChapterVo chapterVo);
	
	void saveByList(List<ChapterVo> chapterVos);

    ChapterVo findTotalByBookId(Integer bookId);

    Integer findChapterByBookId(Integer id, UsersVo usersVo);

    ChapterVo findChapterContentByBookIdAndChapter(Integer booksId, Integer chapterId);

    Integer findPrevChapter(Integer bookId, Integer id);

	Integer findNextChapter(Integer bookId, Integer id);
}