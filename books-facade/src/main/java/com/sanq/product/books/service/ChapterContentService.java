package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.ChapterContentVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface ChapterContentService {
	
	int save(ChapterContentVo chapterContentVo);
	
	int delete(ChapterContentVo chapterContentVo);
	
	int update(ChapterContentVo chapterContentVo, Integer id);
	
	ChapterContentVo findById(Integer id);
	
	List<ChapterContentVo> findList(ChapterContentVo chapterContentVo);
	
	Pager<ChapterContentVo> findListByPage(ChapterContentVo chapterContentVo, Pagination pagination);
	
	int findCount(ChapterContentVo chapterContentVo);
	
	void saveByList(List<ChapterContentVo> chapterContentVos);
}