package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.BlockJoinBooks;

import java.util.List;

public class BlockJoinBooksVo extends BlockJoinBooks{

	/**
	 *	version: 版块和小说关联扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-11
	 */
	private static final long serialVersionUID = 1L;

	private BooksVo booksVo;

	private List<Integer> booksIds;

	public List<Integer> getBooksIds() {
		return booksIds;
	}

	public void setBooksIds(List<Integer> booksIds) {
		this.booksIds = booksIds;
	}

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}
}
