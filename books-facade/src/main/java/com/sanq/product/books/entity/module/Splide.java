package com.sanq.product.books.entity.module;

import java.io.Serializable;

/**
 * com.sanq.product.books.entity.module.Splide
 *
 * @author sanq.Yan
 * @date 2019/8/25
 */
public class Splide implements Serializable {

    private String name;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
