package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class SignIn extends Base implements Serializable {

	/**
	 *	version: 签到表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户ID*/
	private Integer userId;
	/**签到日期*/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date signDay;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getSignDay() {
		return signDay;
	}
	public void setSignDay(Date signDay) {
		this.signDay = signDay;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
