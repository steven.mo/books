package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;

import java.io.Serializable;

public class BooksShelf  extends Base implements Serializable {

	/**
	 *	version: 书架表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-18
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户ID*/
	private Integer userId;
	/**小说ID*/
	private Integer bookId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBookId() {
		return bookId;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}
}
