package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BaseDataVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface BaseDataService {
	
	int save(BaseDataVo baseDataVo);
	
	int delete(BaseDataVo baseDataVo);
	
	int update(BaseDataVo baseDataVo, Integer id);
	
	BaseDataVo findById(Integer id);
	
	List<BaseDataVo> findList(BaseDataVo baseDataVo);
	
	Pager<BaseDataVo> findListByPage(BaseDataVo baseDataVo, Pagination pagination);
	
	int findCount(BaseDataVo baseDataVo);
	
	void saveByList(List<BaseDataVo> baseDataVos);


}