package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.AreaReportVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface AreaReportService {
	
	int save(AreaReportVo areaReportVo);
	
	int delete(AreaReportVo areaReportVo);
	
	int update(AreaReportVo areaReportVo, Integer id);
	
	AreaReportVo findById(Integer id);
	
	List<AreaReportVo> findList(AreaReportVo areaReportVo);
	
	Pager<AreaReportVo> findListByPage(AreaReportVo areaReportVo, Pagination pagination);
	
	int findCount(AreaReportVo areaReportVo);
	
	void saveByList(List<AreaReportVo> areaReportVos);
}