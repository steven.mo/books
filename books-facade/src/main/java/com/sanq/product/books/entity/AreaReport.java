package com.sanq.product.books.entity;

import java.io.Serializable;
import java.util.*;
import java.math.BigDecimal;
import org.springframework.format.annotation.DateTimeFormat;

public class AreaReport  implements Serializable {

	/**
	 *	version: 区域统计报表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-09-05
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date day;
	/***/
	private Integer views;
	/***/
	private Integer ipViews;
	/***/
	private String cityName;
	/***/
	private String provName;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDay() {
		return day;
	}
	public void setDay(Date day) {
		this.day = day;
	}

	public Integer getViews() {
		return views;
	}
	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getIpViews() {
		return ipViews;
	}
	public void setIpViews(Integer ipViews) {
		this.ipViews = ipViews;
	}

	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getProvName() {
		return provName;
	}
	public void setProvName(String provName) {
		this.provName = provName;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
