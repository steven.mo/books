package com.sanq.product.books.utils;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.sanq.product.books.config.Enums;
import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.module.Splide;
import com.sanq.product.books.entity.vo.GlobalSettingVo;
import com.sanq.product.books.service.GlobalSettingService;
import com.sanq.product.config.utils.date.LocalDateUtils;
import com.sanq.product.config.utils.web.GlobalUtil;
import com.sanq.product.security.enums.SecurityFieldEnum;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

/**
 * com.sanq.product.books.utils.WebUtil
 *
 * @author sanq.Yan
 * @date 2019/7/15
 */
public class WebUtil {
    private WebUtil () {
        throw new IllegalStateException("WebUtil Is No Init");
    }

    public static String getToken (HttpServletRequest request) {
        return request.getParameter(SecurityFieldEnum.TOKEN.getName());
    }

    public static ServiceModule getModule(Integer userId) {

        return new ServiceModule.Builder()
                .setDayStart(LocalDateUtils.dayStartTime())
                .setDayEnd(LocalDateUtils.dayEndTime())
                .setLastDayStart(LocalDateUtils.lastDayStartTime())
                .setLastDayEnd(LocalDateUtils.lastDayEndTime())
                .setMonthDayStart(LocalDateUtils.monthStartTime())
                .setMonthDayEnd(LocalDateUtils.monthEndTime())
                .setLastMonthDayStart(LocalDateUtils.lastMonthStartTime())
                .setLastMonthDayEnd(LocalDateUtils.lastMonthEndTime())
                .setUserId(userId)
                .build();
    }

    public static GlobalSettingVo getSetting(GlobalSettingService globalSettingService, String type) {
        GlobalSettingVo globalSettingVo = new GlobalSettingVo();
        globalSettingVo.setSettingType(type);

        List<GlobalSettingVo> list = globalSettingService.findList(globalSettingVo);

        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        return null;
    }
}
