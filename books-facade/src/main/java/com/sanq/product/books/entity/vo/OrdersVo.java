package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Orders;
import com.sanq.product.config.utils.date.DateUtil;
import com.sanq.product.config.utils.date.LocalDateUtils;

import java.util.Date;

public class OrdersVo extends Orders{

	/**
	 *	version: 扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private Integer productId;

	private UsersVo usersVo;

	private String[] times;
	private Date startTime;
	private Date endTime;

	public Date getStartTime() {
		if (times != null && times.length == 2)
			return DateUtil.str2Date(times[0], DATE_FORMAT);
		return LocalDateUtils.dayStartTime();
	}

	public Date getEndTime() {
		if (times != null && times.length == 2)
			return DateUtil.str2Date(times[1], DATE_FORMAT);
		return LocalDateUtils.dayEndTime();
	}

	public void setTimes(String[] times) {
		this.times = times;
	}

	public String[] getTimes() {
		if (times == null || times.length <= 0)
			return new String[]{LocalDateUtils.dayStartTimeStr(), LocalDateUtils.dayEndTimeStr()};
		return times;
	}

	public UsersVo getUsersVo() {
		return usersVo;
	}

	public void setUsersVo(UsersVo usersVo) {
		this.usersVo = usersVo;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}
}
