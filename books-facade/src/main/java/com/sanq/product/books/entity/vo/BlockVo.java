package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Block;
import com.sanq.product.config.utils.entity.Pager;

public class BlockVo extends Block{

	/**
	 *	version: 版块扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private String typeId;

	private Pager<BooksVo> booksVos;

	public Pager<BooksVo> getBooksVos() {
		return booksVos;
	}

	public void setBooksVos(Pager<BooksVo> booksVos) {
		this.booksVos = booksVos;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
}
