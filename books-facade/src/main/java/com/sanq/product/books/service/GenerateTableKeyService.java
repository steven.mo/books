package com.sanq.product.books.service;

/**
 * Created by XieZhyan on 2018/11/10.
 */
public interface GenerateTableKeyService {
    public static final String KEY = "TABLE_KEY";

    Integer getTableKey(String name);
}
