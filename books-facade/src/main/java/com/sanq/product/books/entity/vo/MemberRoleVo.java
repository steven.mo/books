package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.MemberRole;

import java.util.List;

public class MemberRoleVo extends MemberRole {

	/**
	 *	version: 扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-05-21
	 */
	private static final long serialVersionUID = 1L;

	private List<Integer> roleIds;

	public List<Integer> getRoleIds() {
		return roleIds;
	}

	public void setRoleIds(List<Integer> roleIds) {
		this.roleIds = roleIds;
	}
}
