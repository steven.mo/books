package com.sanq.product.books.entity;

import com.sanq.product.config.utils.entity.Base;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class History extends Base implements Serializable {

	/**
	 *	version: 阅读历史
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户ID*/
	private Integer userId;
	/**小说ID*/
	private Integer bookId;
	/**阅读日期*/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date readDay;
	/**阅读章节ID*/
	private Integer readChapterId;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getBookId() {
		return bookId;
	}
	public void setBookId(Integer bookId) {
		this.bookId = bookId;
	}

	public Date getReadDay() {
		return readDay;
	}
	public void setReadDay(Date readDay) {
		this.readDay = readDay;
	}

	public Integer getReadChapterId() {
		return readChapterId;
	}
	public void setReadChapterId(Integer readChapterId) {
		this.readChapterId = readChapterId;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
