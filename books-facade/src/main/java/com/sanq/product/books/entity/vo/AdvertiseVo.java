package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Advertise;

public class AdvertiseVo extends Advertise{

	/**
	 *	version: 广告信息扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	//新增字段
	private String imgUrl;

	private String target;

	private Integer canInSite;

	private Integer weight;

	//1. 直连   2. js
	private Integer advLoadType;

	private String parentDataKey;

	public String getParentDataKey() {
		return parentDataKey;
	}

	public void setParentDataKey(String parentDataKey) {
		this.parentDataKey = parentDataKey;
	}

	public Integer getAdvLoadType() {
		return advLoadType;
	}

	public void setAdvLoadType(Integer advLoadType) {
		this.advLoadType = advLoadType;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Integer getCanInSite() {
		return canInSite;
	}

	public void setCanInSite(Integer canInSite) {
		this.canInSite = canInSite;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
}
