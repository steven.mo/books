package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.SortVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SortService {
	
	int save(SortVo sortVo);
	
	int delete(SortVo sortVo);
	
	int update(SortVo sortVo, Integer id);
	
	SortVo findById(Integer id);
	
	List<SortVo> findList(SortVo sortVo);
	
	Pager<SortVo> findListByPage(SortVo sortVo, Pagination pagination);
	
	int findCount(SortVo sortVo);
	
	void saveByList(List<SortVo> sortVos);
}