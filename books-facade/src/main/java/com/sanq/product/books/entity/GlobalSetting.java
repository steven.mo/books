package com.sanq.product.books.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class GlobalSetting  implements Serializable {

	/**
	 *	version: 全局配置
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-08-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**base, service*/
	private String settingType;
	/***/
	private String settingContent;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getSettingType() {
		return settingType;
	}
	public void setSettingType(String settingType) {
		this.settingType = settingType;
	}

	public String getSettingContent() {
		return settingContent;
	}
	public void setSettingContent(String settingContent) {
		this.settingContent = settingContent;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
