package com.sanq.product.books.entity;

import java.io.Serializable;

public class Sign  implements Serializable {

	/**
	 *	version: 签到表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-25
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**用户ID*/
	private Integer userId;
	/***/
	private String lastSignTime;
	/**连续签到天数*/
	private Integer signCount;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLastSignTime() {
		return lastSignTime;
	}

	public void setLastSignTime(String lastSignTime) {
		this.lastSignTime = lastSignTime;
	}

	public Integer getSignCount() {
		return signCount;
	}
	public void setSignCount(Integer signCount) {
		this.signCount = signCount;
	}

}
