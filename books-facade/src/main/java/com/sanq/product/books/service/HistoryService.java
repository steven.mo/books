package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.HistoryVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface HistoryService {
	
	int save(HistoryVo historyVo);
	
	int delete(HistoryVo historyVo);
	
	int update(HistoryVo historyVo, Integer id);
	
	HistoryVo findById(Integer id);
	
	List<HistoryVo> findList(HistoryVo historyVo);
	
	Pager<HistoryVo> findListByPage(HistoryVo historyVo, Pagination pagination);
	
	int findCount(HistoryVo historyVo);
	
	void saveByList(List<HistoryVo> historyVos);
}