package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.MemberVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;

import java.util.List;

public interface MemberService {
	
	int save(MemberVo memberVo);
	
	int delete(MemberVo memberVo);
	
	int update(MemberVo memberVo, Integer id);
	
	MemberVo findById(Integer id);
	
	List<MemberVo> findList(MemberVo memberVo);
	
	Pager<MemberVo> findListByPage(MemberVo memberVo, Pagination pagination);
	
	int findCount(MemberVo memberVo);
	
	void saveByList(List<MemberVo> memberVos);

    MemberVo login(MemberVo memberVo);

}