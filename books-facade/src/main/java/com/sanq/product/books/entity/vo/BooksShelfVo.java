package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.BooksShelf;

public class BooksShelfVo extends BooksShelf{

	/**
	 *	version: 书架表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-18
	 */
	private static final long serialVersionUID = 1L;

	private HistoryVo historyVo;

	private BooksVo booksVo;

	private Double step;

	private Integer chapterId;

	public Integer getChapterId() {
		return chapterId;
	}

	public void setChapterId(Integer chapterId) {
		this.chapterId = chapterId;
	}

	public Double getStep() {
		return step;
	}

	public void setStep(Double step) {
		this.step = step;
	}

	public HistoryVo getHistoryVo() {
		return historyVo;
	}

	public void setHistoryVo(HistoryVo historyVo) {
		this.historyVo = historyVo;
	}

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}
}
