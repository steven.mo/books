package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.PayHistory;

public class PayHistoryVo extends PayHistory{

	/**
	 *	version: 消费记录扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private BooksVo books;
	private ChapterVo chapter;

	public BooksVo getBooks() {
		return books;
	}

	public void setBooks(BooksVo books) {
		this.books = books;
	}

	public ChapterVo getChapter() {
		return chapter;
	}

	public void setChapter(ChapterVo chapter) {
		this.chapter = chapter;
	}
}
