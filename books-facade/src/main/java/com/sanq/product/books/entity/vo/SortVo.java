package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Sort;

import java.util.List;

public class SortVo extends Sort{

	/**
	 *	version: 分类扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private SortVo parentVo;

	private List<SortVo> sortVoList;

	public SortVo getParentVo() {
		return parentVo;
	}

	public void setParentVo(SortVo parentVo) {
		this.parentVo = parentVo;
	}

	public List<SortVo> getSortVoList() {
		return sortVoList;
	}

	public void setSortVoList(List<SortVo> sortVoList) {
		this.sortVoList = sortVoList;
	}
}
