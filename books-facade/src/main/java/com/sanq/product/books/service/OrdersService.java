package com.sanq.product.books.service;

import com.sanq.product.books.entity.module.ServiceModule;
import com.sanq.product.books.entity.vo.OrdersVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;
import java.util.Map;

public interface OrdersService {
	
	int save(OrdersVo ordersVo);
	
	int delete(OrdersVo ordersVo);
	
	int update(OrdersVo ordersVo, Integer id);
	
	OrdersVo findById(Integer id);
	
	List<OrdersVo> findList(OrdersVo ordersVo);
	
	Pager<OrdersVo> findListByPage(OrdersVo ordersVo, Pagination pagination);
	
	int findCount(OrdersVo ordersVo);
	
	void saveByList(List<OrdersVo> ordersVos);

	/**
	 * 订单汇总
	 * @param module
	 * @return
	 */
    Map<String, Integer> findSummaryOrder(ServiceModule module);

	/**
	 * 查询前10条记录 新增订单折线图
	 * @return
	 */
	List<Map<String, Object>> findLineDataGroupTime();

	/**
	 * 查询前10条记录 新增金额折线图
	 * @return
	 */
	List<Map<String, Object>> findTotalMoneyLineGroupTime();
}