package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BooksVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface BooksService {
	
	int save(BooksVo booksVo);
	
	int delete(BooksVo booksVo);
	
	int update(BooksVo booksVo, Integer id);
	
	BooksVo findById(Integer id);
	
	List<BooksVo> findList(BooksVo booksVo);
	
	Pager<BooksVo> findListByPage(BooksVo booksVo, Pagination pagination);
	
	int findCount(BooksVo booksVo);
	
	void saveByList(List<BooksVo> booksVos);

	//-----------------------
	Pager<BooksVo> findBooksByBlockId(Integer blockId, Pagination pagination);

    Pager<BooksVo> findListBySortIds(List<String> sortIds, Pagination pagination);
}