package com.sanq.product.books.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class Books  implements Serializable {

	/**
	 *	version: 小说
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/**小说名称*/
	private String booksName;
	/**作者*/
	private String author;
	/**分类*/
	private Integer sortId;
	/**0.未完结 1.已完结*/
	private String isOver;
	/**封面*/
	private String booksCover;
	/**小说备注*/
	private String booksDescription;
	/**阅读人群性别*/
	private String readerSex;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getBooksName() {
		return booksName;
	}
	public void setBooksName(String booksName) {
		this.booksName = booksName;
	}

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}



	public String getBooksCover() {
		return booksCover;
	}
	public void setBooksCover(String booksCover) {
		this.booksCover = booksCover;
	}

	public String getBooksDescription() {
		return booksDescription;
	}
	public void setBooksDescription(String booksDescription) {
		this.booksDescription = booksDescription;
	}

	public Integer getSortId() {
		return sortId;
	}

	public void setSortId(Integer sortId) {
		this.sortId = sortId;
	}

	public String getIsOver() {
		return isOver;
	}

	public void setIsOver(String isOver) {
		this.isOver = isOver;
	}

	public String getReaderSex() {
		return readerSex;
	}

	public void setReaderSex(String readerSex) {
		this.readerSex = readerSex;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
