package com.sanq.product.books.entity;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

public class OsReport  implements Serializable {

	/**
	 *	version: 设备统计报表
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-09-05
	 */
	private static final long serialVersionUID = 1L;
	
	/***/
	private Integer id;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date day;
	/***/
	private Integer views;
	/***/
	private Integer ipViews;
	/***/
	private String os;
	/***/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Date getDay() {
		return day;
	}
	public void setDay(Date day) {
		this.day = day;
	}

	public Integer getViews() {
		return views;
	}
	public void setViews(Integer views) {
		this.views = views;
	}

	public Integer getIpViews() {
		return ipViews;
	}
	public void setIpViews(Integer ipViews) {
		this.ipViews = ipViews;
	}

	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}

	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
