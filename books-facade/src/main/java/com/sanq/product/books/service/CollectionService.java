package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.CollectionVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface CollectionService {
	
	int save(CollectionVo collectionVo);
	
	int delete(CollectionVo collectionVo);
	
	int update(CollectionVo collectionVo, Integer id);
	
	CollectionVo findById(Integer id);
	
	List<CollectionVo> findList(CollectionVo collectionVo);
	
	Pager<CollectionVo> findListByPage(CollectionVo collectionVo, Pagination pagination);
	
	int findCount(CollectionVo collectionVo);
	
	void saveByList(List<CollectionVo> collectionVos);
}