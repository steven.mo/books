package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Collection;

public class CollectionVo extends Collection{

	/**
	 *	version: 收藏扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private boolean collection;

	private BooksVo booksVo;

	public BooksVo getBooksVo() {
		return booksVo;
	}

	public void setBooksVo(BooksVo booksVo) {
		this.booksVo = booksVo;
	}

	public boolean isCollection() {
		return collection;
	}

	public void setCollection(boolean collection) {
		this.collection = collection;
	}
}
