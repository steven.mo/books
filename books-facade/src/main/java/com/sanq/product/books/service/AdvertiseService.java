package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.AdvertiseVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface AdvertiseService {
	
	int save(AdvertiseVo advertiseVo);
	
	int delete(AdvertiseVo advertiseVo);
	
	int update(AdvertiseVo advertiseVo, Integer id);
	
	AdvertiseVo findById(Integer id);
	
	List<AdvertiseVo> findList(AdvertiseVo advertiseVo);
	
	Pager<AdvertiseVo> findListByPage(AdvertiseVo advertiseVo, Pagination pagination);
	
	int findCount(AdvertiseVo advertiseVo);
	
	void saveByList(List<AdvertiseVo> advertiseVos);
}