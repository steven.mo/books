package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.BaseReportVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;
import java.util.Map;

public interface BaseReportService {
	
	int save(BaseReportVo baseReportVo);
	
	int delete(BaseReportVo baseReportVo);
	
	int update(BaseReportVo baseReportVo, Integer id);
	
	BaseReportVo findById(Integer id);
	
	List<BaseReportVo> findList(BaseReportVo baseReportVo);
	
	Pager<BaseReportVo> findListByPage(BaseReportVo baseReportVo, Pagination pagination);
	
	int findCount(BaseReportVo baseReportVo);
	
	void saveByList(List<BaseReportVo> baseReportVos);

    List<Map<String, Object>> findLineDataGroupTime(char a);
}