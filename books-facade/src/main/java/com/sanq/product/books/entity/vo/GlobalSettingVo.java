package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.GlobalSetting;
import com.sanq.product.books.entity.module.Splide;

import java.util.List;

public class GlobalSettingVo extends GlobalSetting{

	/**
	 *	version: 全局配置扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-08-09
	 */
	private static final long serialVersionUID = 1L;

	private List<Splide> splides;

	public List<Splide> getSplides() {
		return splides;
	}

	public void setSplides(List<Splide> splides) {
		this.splides = splides;
	}
}
