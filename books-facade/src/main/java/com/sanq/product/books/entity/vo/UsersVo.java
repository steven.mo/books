package com.sanq.product.books.entity.vo;

import com.sanq.product.books.entity.Users;
import com.sanq.product.config.utils.string.DigestUtil;

public class UsersVo extends Users{

	/**
	 *	version: 用户表扩展实体
	 *----------------------
	 * 	author:xiezhyan
	 * 	date:2019-07-09
	 */
	private static final long serialVersionUID = 1L;

	private String loginName;

	private String code;

	private String type;

	private Integer collectionCount;
	private Integer discussCount;

	private Integer vipEndDay;

	public Integer getVipEndDay() {
		return vipEndDay;
	}

	public void setVipEndDay(Integer vipEndDay) {
		this.vipEndDay = vipEndDay;
	}

	public Integer getCollectionCount() {
		return collectionCount;
	}

	public void setCollectionCount(Integer collectionCount) {
		this.collectionCount = collectionCount;
	}

	public Integer getDiscussCount() {
		return discussCount;
	}

	public void setDiscussCount(Integer discussCount) {
		this.discussCount = discussCount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	private String decodeTel;

	public String getDecodeTel() {
		return DigestUtil.getInstance().decode(getTel());
	}
}
