package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.NoticeVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface NoticeService {
	
	int save(NoticeVo noticeVo);
	
	int delete(NoticeVo noticeVo);
	
	int update(NoticeVo noticeVo, Integer id);
	
	NoticeVo findById(Integer id);
	
	List<NoticeVo> findList(NoticeVo noticeVo);
	
	Pager<NoticeVo> findListByPage(NoticeVo noticeVo,Pagination pagination);
	
	int findCount(NoticeVo noticeVo);
	
	void saveByList(List<NoticeVo> noticeVos);
}