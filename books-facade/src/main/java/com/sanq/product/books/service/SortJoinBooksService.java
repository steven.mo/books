package com.sanq.product.books.service;

import com.sanq.product.books.entity.vo.SortJoinBooksVo;
import com.sanq.product.config.utils.entity.Pager;
import com.sanq.product.config.utils.entity.Pagination;
import java.util.List;

public interface SortJoinBooksService {
	
	int save(SortJoinBooksVo sortJoinBooksVo);
	
	int delete(SortJoinBooksVo sortJoinBooksVo);
	
	int update(SortJoinBooksVo sortJoinBooksVo, Integer id);
	
	SortJoinBooksVo findById(Integer id);
	
	List<SortJoinBooksVo> findList(SortJoinBooksVo sortJoinBooksVo);
	
	Pager<SortJoinBooksVo> findListByPage(SortJoinBooksVo sortJoinBooksVo, Pagination pagination);
	
	int findCount(SortJoinBooksVo sortJoinBooksVo);
	
	void saveByList(List<SortJoinBooksVo> sortJoinBooksVos);

    List<Integer> getSortIdByBooksId(Integer booksId);

	String findSortIdList(Integer booksId);

}