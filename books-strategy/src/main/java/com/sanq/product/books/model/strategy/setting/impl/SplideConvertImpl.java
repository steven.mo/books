package com.sanq.product.books.model.strategy.setting.impl;

import com.sanq.product.books.entity.module.Splide;
import com.sanq.product.books.model.strategy.setting.SettingConvertInterface;
import com.sanq.product.config.utils.web.JsonUtil;

/**
 * com.sanq.product.books.model.strategy.setting.impl.SplideConvertImpl
 *
 * @author sanq.Yan
 * @date 2019/8/25
 */
public class SplideConvertImpl implements SettingConvertInterface {


    @Override
    public Object convert(String data) {
        return JsonUtil.json2ObjList(data, Splide.class);
    }
}
