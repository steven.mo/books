package com.sanq.product.books.model.strategy.setting;

/**
 * com.sanq.product.books.model.strategy.setting.SettingEnum
 *
 * @author sanq.Yan
 * @date 2019/8/25
 */
public enum SettingEnum {
    splide("com.sanq.product.books.model.strategy.setting.impl.SplideConvertImpl");

    private String clazz;

    SettingEnum(String clazz) {
        this.clazz = clazz;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }}
