package com.sanq.product.books.model.strategy.setting;

/**
 * com.sanq.product.books.model.strategy.setting.SettingConvertInterface
 *
 * @author sanq.Yan
 * @date 2019/8/25
 */
public interface SettingConvertInterface {

    Object convert(String data);

}
