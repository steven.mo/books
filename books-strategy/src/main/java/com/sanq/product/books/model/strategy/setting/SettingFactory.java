package com.sanq.product.books.model.strategy.setting;

/**
 * com.sanq.product.books.model.strategy.setting.SettingFactory
 *
 * @author sanq.Yan
 * @date 2019/8/25
 */
public class SettingFactory {

    private SettingFactory() {}

    public static SettingFactory getInstance() {
        return Holder.INSTANCE;
    }

    private static class Holder {
        private static final SettingFactory INSTANCE = new SettingFactory();
    }

    public SettingConvertInterface getFactory(String type) {
        String className = SettingEnum.valueOf(type).getClazz();

        try {
            return (SettingConvertInterface) Class.forName(className).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
