export const navigationData = [
    // { index: 1, type: 'icon', icon: 'cubeic-person', value: '我的' },
    // { index: 2, type: 'img', icon: require('assets/img/qy_tab_sel_f_04.png'), activeIcon: require('assets/img/qy_tab_sel_t_04.png'), value: '我的' }
    { index: 1, type: 'icon', icon: 'iconfont icon-shelf', activeIcon: 'iconfont icon-shujia', value: '书架' },
    { index: 2, type: 'icon', icon: 'iconfont icon-shouye1', activeIcon: 'iconfont icon-shouye', value: '书城' },
    { index: 4, type: 'icon', icon: 'iconfont icon-M', activeIcon: 'iconfont icon-manhuadao1', value: '漫画' },
    { index: 3, type: 'icon', icon: 'iconfont icon-wode1', activeIcon: 'iconfont icon-wode', value: '我的' },
]

export const config = {
    banner: {
        concentration: "1.1.1.1",
        man: '1.1.1.2',
        woman: '1.1.1.3',
        advertise: '1.1.2.1',
        read_advertise: '1.1.2.2',
        txt_read_advertise: '1.1.2.3'
    },
    blockType: {
        concent: '2.6.1',
        man: '2.6.2',
        woman: '2.6.3'
    },
    block: {
        high: 1,
        good: 2,
        starter: 3,
        work: 4,
        all_look: 5,
        gathering: 6,
        free: 7,
        new_book: 8,
        man_reader: 9,
        man_look: 10,
        pressure: 11,
        new_read: 12,
        hot_over: 13,
        woman_look: 14,
        woman_reader: 15
    },
    errorCodes: {
        NO_TOKEN: 400
    },
    isOver: {
        Over: '3.11.1',
        No_Over: '3.11.2'
    },
    module: {
        UTILS: 'UTILS'
    },
    notice: {
        ALL: "ALL",
        NO_READER: "4.15.1",
        READER: "4.15.2",
        COLL_NEWS: '4.15.4'
    },
    suggestionId: 20
}