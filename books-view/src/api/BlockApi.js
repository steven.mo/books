import $axios from 'utils/Request.js'

//通过 blockId 查询图书
export const findBooksByBlockId = data => {
    return $axios({
        url: '/app/block/get/' + data.id,
        method: 'get',
        data
    });
}