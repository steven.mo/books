import $axios from 'utils/Request.js'


export const saveBooksToShelf = data => {
    return $axios({
        url: '/app/books_shelf/save',
        method: 'post',
        data
    });
}

export const getBooksShelfList = data => {
    return $axios({
        url: '/app/books_shelf/all',
        method: 'get',
        data
    });
}

export const deleteBooksShelf = data => {
    return $axios({
        url: '/app/books_shelf/delete',
        method: 'delete',
        data
    });
}
