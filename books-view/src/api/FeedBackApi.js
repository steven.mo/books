import $axios from 'utils/Request.js'

//列表分页
export const getFeedBackList = data => {
    return $axios({
        url: '/api/feed_back/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getFeedBackAllList = data => {
    return $axios({
        url: '/api/feed_back/all',
        method: 'get',
        data
    });
}

//保存
export const saveFeedBack = data => {
    return $axios({
        url: '/api/feed_back/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteFeedBack = data => {
    return $axios({
        url: '/api/feed_back/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateFeedBackById = data => {
    return $axios({
        url: '/api/feed_back/update/' + data.id,
        method: 'put',
        data
    });
}