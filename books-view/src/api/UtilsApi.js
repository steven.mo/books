import $axios from 'utils/Request.js'

// 发送验证码
export const sendSmsCode = data => {
    return $axios({
        url: '/api/sms/send',
        method: 'get',
        data
    });
}


export const deleteFile = data => {
    return $axios({
        url: '/api/file/delete',
        method: 'post',
        data
    });
}
