import $axios from 'utils/Request.js'

//列表分页
export const getPayHistoryList = data => {
    return $axios({
        url: '/app/pay_history/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getPayHistoryAllList = data => {
    return $axios({
        url: '/app/pay_history/all',
        method: 'get',
        data
    });
}

//保存
export const savePayHistory = data => {
    return $axios({
        url: '/app/pay_history/save',
        method: 'post',
        data
    });
}

//删除数据
export const deletePayHistory = data => {
    return $axios({
        url: '/app/pay_history/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updatePayHistoryById = data => {
    return $axios({
        url: '/app/pay_history/update/' + data.id,
        method: 'put',
        data
    });
}