import $axios from 'utils/Request.js'

//列表分页
export const getDiscussList = data => {
    return $axios({
        url: '/app/discuss/list',
        method: 'get',
        data
    });
}

//保存
export const saveDiscuss = data => {
    return $axios({
        url: '/app/discuss/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteDiscuss = data => {
    return $axios({
        url: '/app/discuss/delete',
        method: 'delete',
        data
    });
}

//根据小说ID获取书籍评分， 评论数， 人气值
export const findTotalsByBookId = data => {
    return $axios({
        url: '/app/discuss/findTotalsByBookId',
        method: 'get',
        data
    });
}