import $axios from 'utils/Request.js'

//列表分页
export const getBaseDataList = data => {
    return $axios({
        url: '/app/base_data/list',
        method: 'get',
        data
    });
}

//获取所有数据
export const getBaseDataAllList = data => {
    return $axios({
        url: '/app/base_data/all',
        method: 'get',
        data
    });
}

//保存
export const saveBaseData = data => {
    return $axios({
        url: '/app/base_data/save',
        method: 'post',
        data
    });
}

//删除数据
export const deleteBaseData = data => {
    return $axios({
        url: '/app/base_data/delete',
        method: 'delete',
        data
    });
}

//修改数据
export const updateBaseDataById = data => {
    return $axios({
        url: '/app/base_data/update/' + data.id,
        method: 'put',
        data
    });
}