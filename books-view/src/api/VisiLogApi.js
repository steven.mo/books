import $axios from 'utils/Request.js'

export const visiLogSave = data => {
    return $axios({
        url: '/app/visi_log/save',
        method: 'post',
        data
    });
}
