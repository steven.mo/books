import $axios from 'utils/Request.js'

//保存
export const saveSuggestion = data => {
    return $axios({
        url: '/app/suggestions/save',
        method: 'post',
        data
    });
}
