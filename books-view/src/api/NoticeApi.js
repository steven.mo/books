import $axios from 'utils/Request.js'

//获取所有数据
export const getNoticeList = data => {
    return $axios({
        url: '/app/notice/list',
        method: 'get',
        data
    });
}

export const updateNotice = data => {
    return $axios({
        url: '/app/notice/update/' + data.id,
        method: 'put',
        data
    });
}

