import $axios from 'utils/Request.js'

//获取所有数据
export const getProductAllList = data => {
    return $axios({
        url: '/app/product/all',
        method: 'get',
        data
    });
}