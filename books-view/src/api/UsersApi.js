import $axios from 'utils/Request.js'

export const login = data => {
    return $axios({
        url: '/app/users/login',
        method: 'post',
        data
    });
}

export const reg = data => {
    return $axios({
        url: '/app/users/reg',
        method: 'post',
        data
    });
}

export const updateUsers = data => {
    return $axios({
        url: '/app/users/update',
        method: 'post',
        data
    });
}

export const getUsersForCache = data => {
    return $axios({
        url: '/app/users/getUsersForCache',
        method: 'get',
        data
    });
}
