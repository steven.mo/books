import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/books/shop'
    },
    {
      path: '/books/shop',
      name: 'BooksShop',
      component: resolve => require(['@/views/books/shop/Index.vue'], resolve),
      meta: {
        title: "书城"
      }
    },
    {
      path: '/books/detail/:bookId',
      name: 'DetailIndex',
      component: resolve => require(['@/views/books/detail/Index.vue'], resolve),
      meta: {
        title: "小说详情页"
      }
    },
    {
      path: '/books/chapter/:bookId',
      name: 'ChapterIndex',
      component: resolve => require(['@/views/books/chapter/Index.vue'], resolve),
      meta: {
        title: "章节目录"
      }
    },
    {
      path: '/books/discuss/:bookId/list',
      name: 'DiscussList',
      component: resolve => require(['@/views/books/discuss/DiscussList.vue'], resolve),
      meta: {
        title: "全部书评"
      }
    },
    {
      path: '/books/discuss/:bookId/edit',
      name: 'DiscussEdit',
      component: resolve => require(['@/views/books/discuss/DiscussEdit.vue'], resolve),
      meta: {
        title: "写书评"
      }
    },
    {
      path: '/search',
      name: 'SearchIndex',
      component: resolve => require(['@/views/books/search/Index.vue'], resolve),
      meta: {
        title: "搜索界面"
      }
    },
    {
      path: '/list/shelf',
      name: 'ShelfIndex',
      component: resolve => require(['@/views/books/shelf/Index.vue'], resolve),
      meta: {
        title: "我的书架"
      }
    },
    {
      path: '/list/sort',
      name: 'SortIndex',
      component: resolve => require(['@/views/books/sort/Index.vue'], resolve),
      meta: {
        title: "分类"
      }
    },
    {
      path: '/list/books/:sortId/:childId',
      name: 'BooksList',
      component: resolve => require(['@/views/books/books/Index.vue'], resolve),
      meta: {
        title: "小说列表"
      }
    },

    {
      path: '/users/center',
      name: 'UsersCenterIndex',
      component: resolve => require(['@/views/users/users/users/Index.vue'], resolve),
      meta: {
        title: "我的"
      }
    },
    {
      path: '/users/info',
      name: 'UsersInfoIndex',
      component: resolve => require(['@/views/users/users/info/Index.vue'], resolve),
      meta: {
        title: "个人信息"
      }
    },
    {
      path: '/users/edit/:text',
      name: 'EditTextIndex',
      component: resolve => require(['@/views/users/users/edit/Edit.vue'], resolve),
      meta: {
        title: "修改个人信息"
      },
      props: true
    },
    {
      path: '/login',
      name: 'Login',
      component: resolve => require(['@/views/users/login/Login.vue'], resolve),
      meta: {
        title: "登陆"
      }
    },
    {
      path: '/reg',
      name: 'Reg',
      component: resolve => require(['@/views/users/reg/Index.vue'], resolve),
      meta: {
        title: "注册"
      }
    },
    {
      path: '/user/sign',
      name: 'Sign',
      component: resolve => require(['@/views/users/sign/Index.vue'], resolve),
      meta: {
        title: "一日打卡"
      }
    },
    {
      path: '/user/list/history',
      name: 'HistoryList',
      component: resolve => require(['@/views/users/users/history/Index.vue'], resolve),
      meta: {
        title: "阅读历史"
      }
    },
    {
      path: '/user/list/collection',
      name: 'CollectionList',
      component: resolve => require(['@/views/users/users/collection/Index.vue'], resolve),
      meta: {
        title: "我的收藏"
      }
    },
    {
      path: '/user/wallet',
      name: 'WalletIndex',
      component: resolve => require(['@/views/pays/wallet/Index.vue'], resolve),
      meta: {
        title: "我的钱包"
      }
    },
    {
      path: '/list/pay',
      name: 'PayIndex',
      component: resolve => require(['@/views/pays/pay/Index.vue'], resolve),
      meta: {
        title: "充值"
      }
    },
    {
      path: '/list/notice',
      name: 'NoticeIndex',
      component: resolve => require(['@/views/notice/Index.vue'], resolve),
      meta: {
        title: "消息中心"
      }
    },
    {
      path: '/user/list/discuss',
      name: 'DiscussIndex',
      component: resolve => require(['@/views/users/users/discuss/Index.vue'], resolve),
      meta: {
        title: "我的书评"
      }
    },
    {
      path: '/books/reader/:booksId/:chapterId',
      name: 'ReaderIndex',
      component: resolve => require(['@/views/reader/Index'], resolve),
      meta: {
        title: "阅读器"
      },
      props: true
    },
    {
      path: '/user/suggestions',
      name: 'SuggestionsIndex',
      component: resolve => require(['@/views/users/suggestions/Index'], resolve),
      meta: {
        title: "意见反馈"
      },
      props: true
    },
    {
      path: '/user/pay/history',
      name: 'PayHistory',
      component: resolve => require(['@/views/pays/history/pay/Index'], resolve),
      meta: {
        title: "充值记录"
      }
    },
    {
      path: '/user/consume/history',
      name: 'ConsumeHistory',
      component: resolve => require(['@/views/pays/history/consume/Index'], resolve),
      meta: {
        title: "消费记录"
      }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (savedPosition) {
          resolve(savedPosition)
        } else {
          resolve({ x: 0, y: 0 })
        }
        
      }, 500)
    })
  }
})
