import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let token="",
  usersCenterTag = "hide",
  searchHistoryList = "",
  fs = 16,
  colors = 'default';
try {
  if(localStorage.token) {
    token = localStorage.token
  }
  if(localStorage.usersCenterTag) {
    usersCenterTag = localStorage.usersCenterTag
  }

  if(localStorage.searchHistoryList) {
    searchHistoryList = localStorage.searchHistoryList
  }

  if(localStorage.fs) {
    fs = localStorage.fs
  }

  if(localStorage.colors) {
    colors = localStorage.colors
  }

} catch(e) {
}

export default new Vuex.Store({
  state: {
    token: token,
    usersCenterTag: usersCenterTag,
    searchHistoryList: searchHistoryList,
    fs: fs,
    colors: colors
  },
  mutations: {
    saveToken(state, token) {
      state.token = token;
      localStorage.token = token;
    },
    saveUsersCenterTag (state, usersCenterTag) {
      state.usersCenterTag = usersCenterTag;
      localStorage.usersCenterTag = usersCenterTag;
    },
    saveSearchHistory (state, search) {
      let list;
      if(state.searchHistoryList) {
        list = JSON.parse(state.searchHistoryList)
      } else list = []

      list.push(search);

      state.searchHistoryList = JSON.stringify(list)
      localStorage.searchHistoryList = JSON.stringify(list);
    },
    clearSearchHistory(state) {
      state.searchHistoryList = ''
      localStorage.searchHistoryList = '';
    },
    cleanThisStore() {
      localStorage.clear();
    },
    saveFontSize(state, fontSize) {
      state.fs = fontSize;
      localStorage.fs = fontSize;
    },
    saveColors(state, colors) {
      state.colors = colors;
      localStorage.colors = colors;
    }
  },
  actions: {

  }
})
